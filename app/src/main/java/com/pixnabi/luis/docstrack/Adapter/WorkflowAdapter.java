package com.pixnabi.luis.docstrack.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.Fragments.WorkflowFragments.WorkflowFragment;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ImageToString;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Luis on 12/20/2016.
 */

public class WorkflowAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray data;
    MainActivity mainActivity;
    WorkflowFragment workflowFragment;

    public WorkflowAdapter(Context context, WorkflowFragment workflowFragment){
        this.context = context;
        mainActivity = (MainActivity)context;
        this.workflowFragment = workflowFragment;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (data == null) ? 0 : data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_new_workflow, parent, false);

        final TextView name, status, owner, sentOn, dueDate, num, email;
        LinearLayout statusLayout;
        CircleImageView circleImageView;

        name = (TextView)rowView.findViewById(R.id.rowWorkflowDocumentName);
        status = (TextView)rowView.findViewById(R.id.rowWorkflowStatus);
        owner = (TextView)rowView.findViewById(R.id.rowWorkflowOwner);
        sentOn = (TextView)rowView.findViewById(R.id.rowWorkflowSentOn);
        dueDate = (TextView)rowView.findViewById(R.id.rowWorkflowDueDate);
        num = (TextView)rowView.findViewById(R.id.rowWorkflowDocumentNumber);
        email = (TextView)rowView.findViewById(R.id.rowWorkflowEmail);
        statusLayout = (LinearLayout)rowView.findViewById(R.id.rowWorkflowStatusLayout);
        circleImageView = (CircleImageView)rowView.findViewById(R.id.rowWorkflowPicture);

        try {
            name.setText(data.getJSONObject(position).getString("name"));
            num.setText(data.getJSONObject(position).getString("num"));
            email.setText(data.getJSONObject(position).getString("email"));

            if(data.getJSONObject(position).get("status").toString().equals("Awaiting Start")){
                status.setText(data.getJSONObject(position).getString("status"));
                statusLayout.setBackgroundColor(context.getResources().getColor(R.color.grayStatus));
            }else if(data.getJSONObject(position).getString("status").equals("Late") || data.getJSONObject(position).getString("status").equals("Stopped") || data.getJSONObject(position).getString("status").equals("Rejected")){
                status.setText(data.getJSONObject(position).getString("status"));
                statusLayout.setBackgroundColor(context.getResources().getColor(R.color.redStatus));
            }else{
                status.setText(data.getJSONObject(position).getString("status"));
                statusLayout.setBackgroundColor(context.getResources().getColor(R.color.yellowStatus));
            }

            owner.setText(data.getJSONObject(position).getString("owner"));
            sentOn.setText(data.getJSONObject(position).getString("sentDate"));
            dueDate.setText(data.getJSONObject(position).getString("dueDate"));

            circleImageView.setImageBitmap(ImageToString.StringToImage(data.getJSONObject(position).getString("profilePicture")));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((status.getText().toString().equals("Awaiting Start") && !MainActivity.sharedPreferences.getString("role", null).equals("Approver")) || (status.getText().toString().equals("Stopped") && !MainActivity.sharedPreferences.getString("role", null).equals("Approver")) || (status.getText().toString().equals("Rejected") && !MainActivity.sharedPreferences.getString("role", null).equals("Approver"))){
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_start_workflow);
                    Button cancel, start;
                    cancel = (Button)dialog.findViewById(R.id.startWorkflowCancelButton);
                    start = (Button)dialog.findViewById(R.id.startWorkflowStartButton);
                    dialog.show();

                    start.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            workflowFragment.listView.setVisibility(View.INVISIBLE);
                            MainActivity.progressBar.setVisibility(View.VISIBLE);

                            try {
                                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                    @Override
                                    public void processFinish(String result) {

                                        workflowFragment.loadData();

                                        try {
                                            final JSONObject jsonObject = new JSONObject(result);

                                            if(jsonObject.has("pushIds")){

                                                JSONArray pushId = jsonObject.getJSONArray("pushIds");
                                                String pushNotiString = "";

                                                for(int i=0;i<pushId.length();i++){
                                                    if(pushNotiString.equals("")){
                                                        if(!pushId.getString(i).equals("null")){
                                                            pushNotiString = "'" + pushId.getString(i) + "'";
                                                        }
                                                    }else{
                                                        if(!pushId.getString(i).equals("null")){
                                                            pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                                                        }
                                                    }
                                                }

                                                Log.wtf("pushNoti", pushNotiString);

                                                final String finalPushNotiString = pushNotiString;
                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        String pushString = null;
                                                        try {

                                                            pushString = "Document ID " + data.getJSONObject(position).getString("num") + " has started the approval process. You will receive the document for your approval soon. You have " + jsonObject.getString("notifications") + " for the approval.";

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        try {
                                                            OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                                    new OneSignal.PostNotificationResponseHandler() {
                                                                        @Override
                                                                        public void onSuccess(JSONObject response) {
                                                                            Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                                        }

                                                                        @Override
                                                                        public void onFailure(JSONObject response) {
                                                                            Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                                        }
                                                                    });
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }).start();


                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }).execute("WorkflowSet", mainActivity.sharedPreferences.getString("userId",null), mainActivity.sharedPreferences.getString("companyId", null), mainActivity.sharedPreferences.getString("fullName", null), data.getJSONObject(position).getString("id"), "Start Workflow", "", "","");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }else if(status.getText().toString().equals("In Review") || status.getText().toString().equals("Late")){
                    if(owner.getText().toString().equals(mainActivity.sharedPreferences.getString("fullName", null)) || mainActivity.sharedPreferences.getString("role",null).equals("Administrator")){
                        try {
                            mainActivity.workflowAdminorOwner(data.getJSONObject(position).getString("id"), data.getJSONObject(position).getString("name"), data.getJSONObject(position).getString("num"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else if(mainActivity.sharedPreferences.getString("role", null).equals("Approver")){
                        try {
                            mainActivity.workflowApprover(data.getJSONObject(position).getString("id"), data.getJSONObject(position).getString("name"), data.getJSONObject(position).getString("num"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        return rowView;
    }
}
