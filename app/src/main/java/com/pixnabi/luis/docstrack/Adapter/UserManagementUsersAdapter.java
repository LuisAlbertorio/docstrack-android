package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement.EditUserActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.ImageToString;

import org.json.JSONArray;
import org.json.JSONException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Luis on 11/21/2016.
 */

public class UserManagementUsersAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    String myUserId;

    JSONArray userArray;


    public UserManagementUsersAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray userArray, String myUserId){
        this.userArray = userArray;
        this.myUserId = myUserId;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (userArray == null) ? 0 : userArray.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_users, parent, false);

        TextView name, email, role, createdBy;
        CircleImageView profilePicture;
        LinearLayout card;

        name = (TextView)rowView.findViewById(R.id.rowUsersName);
        email = (TextView)rowView.findViewById(R.id.rowUsersEmail);
        role = (TextView)rowView.findViewById(R.id.rowUsersRole);
        createdBy = (TextView)rowView.findViewById(R.id.rowUsersCreatedBy);
        profilePicture = (CircleImageView)rowView.findViewById(R.id.userManagementProfilePicture);
        card = (LinearLayout)rowView.findViewById(R.id.rowUsersLinearLayout);

        try {

            name.setText(userArray.getJSONObject(position).getString("fName") + " " + userArray.getJSONObject(position).getString("lName"));
            email.setText(userArray.getJSONObject(position).getString("uEmail"));
            role.setText(userArray.getJSONObject(position).getString("uRole"));
            createdBy.setText("Created by: " + userArray.getJSONObject(position).getString("uCreatedByFullName"));

            if(userArray.getJSONObject(position).getString("uRole").equals("Administrator")){
                createdBy.setVisibility(View.GONE);
            }

            if(!userArray.getJSONObject(position).getString("uCreatedById").equals(myUserId) && !userArray.getJSONObject(position).getString("uCreatedById").equals("myself"+myUserId)){
                card.setBackgroundColor(context.getResources().getColor(R.color.grayInactive));
            }else if(userArray.getJSONObject(position).getString("uCreatedById").equals(myUserId)){
                rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!MainActivity.sharedPreferences.getString("role", null).equals("Owner")) {
                            Intent intent = new Intent(context, EditUserActivity.class);
                            try {

                                intent.putExtra("userId", userArray.getJSONObject(position).getString("uUserId"));
                                intent.putExtra("fname", userArray.getJSONObject(position).getString("fName"));
                                intent.putExtra("lname", userArray.getJSONObject(position).getString("lName"));
                                context.startActivity(intent);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(context, "You don't have permission to edit users", Toast.LENGTH_LONG).show();
                        }


                    }
                });
            }


            profilePicture.setImageBitmap(ImageToString.StringToImage(userArray.getJSONObject(position).getString("uProfilePicture")));

        } catch (JSONException e) {
            e.printStackTrace();
        }




        return rowView;
    }
}
