package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchUIUtil;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.ItemTouchHelperAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Luis on 12/15/2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> implements ItemTouchHelperAdapter{

    ArrayList<String> info;
    HashMap<String, String> dataHashmap;
    ArrayList<String> dept;
    ArrayList<String> id;
    ArrayList<String> namePlusDept;
    Context context;

    public RecyclerAdapter(Context context){
        this.context = context;
    }

    public void setData(ArrayList<String> info, ArrayList<String> dept, ArrayList<String> id, ArrayList<String> namePlusDept){
        this.info = info;
        this.dept = dept;
        this.id = id;
        this.namePlusDept = namePlusDept;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycle, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(info.get(position));
        holder.dept.setText(dept.get(position));
    }

    @Override
    public int getItemCount() {
        return (info == null) ? 0 : info.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if(fromPosition < toPosition){
            for(int i=fromPosition;i<toPosition;i++){
                Collections.swap(info, i, i+1);
                Collections.swap(dept, i, i+1);
                Collections.swap(id, i, i+1);
                Collections.swap(namePlusDept, i, i+1);
            }
        }else{
            for(int i= fromPosition; i > toPosition; i--){
                Collections.swap(info, i, i-1);
                Collections.swap(dept, i, i-1);
                Collections.swap(id, i, i-1);
                Collections.swap(namePlusDept, i, i-1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
//        Toast.makeText(context, String.valueOf(info), Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
            info.remove(position);
            dept.remove(position);
            id.remove(position);
            namePlusDept.remove(position);
            notifyItemRemoved(position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        protected TextView name;
        protected TextView dept;

        public MyViewHolder(View v){
            super(v);
            name = (TextView)v.findViewById(R.id.recycleTextView);
            dept = (TextView)v.findViewById(R.id.recycleDeptTextView);
        }
    }
}
