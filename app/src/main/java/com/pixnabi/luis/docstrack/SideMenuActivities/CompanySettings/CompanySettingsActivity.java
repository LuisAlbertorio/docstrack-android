package com.pixnabi.luis.docstrack.SideMenuActivities.CompanySettings;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Process;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class CompanySettingsActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ImageView logo;
    Button saveChanges;
    EditText companyName, street, city, state, zip, phone, web;
    Bitmap photo = null;

    ImageButton changePhoto;

    String[] permissions = {Manifest.permission.CAMERA};
    int requestPermission = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_company_settings);
        logo = (ImageView)findViewById(R.id.companySettingsLogo);
        changePhoto = (ImageButton) findViewById(R.id.companySettingsCamera);
        saveChanges = (Button)findViewById(R.id.companySettingsSaveChanges);
        companyName = (EditText)findViewById(R.id.companySettingsCompanyName);
        street = (EditText)findViewById(R.id.companySettingsStreet);
        city = (EditText)findViewById(R.id.companySettingsCity);
        state = (EditText)findViewById(R.id.companySettingsState);
        zip = (EditText)findViewById(R.id.companySettingsZip);
        phone = (EditText)findViewById(R.id.companySettingsPhone);
        web = (EditText)findViewById(R.id.companySettingsWeb);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Getting Company Info...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        registerForContextMenu(changePhoto);

        //TODO: change photo onclick to contextual menu

        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClicked();
            }
        });

        changePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContextMenu(v);
            }
        });

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    companyName.setText(jsonObject.getString("name"));

                    if(!jsonObject.getString("street").equals("null")){
                        street.setText(jsonObject.getString("street"));
                    }

                    if(!jsonObject.getString("city").equals("null")){
                        city.setText(jsonObject.getString("city"));
                    }

                    if(!jsonObject.getString("state").equals("null")){
                        state.setText(jsonObject.getString("state"));
                    }

                    if(!jsonObject.getString("zip").equals("null")){
                        zip.setText(jsonObject.getString("zip"));
                    }

                    if(!jsonObject.getString("phone").equals("null")){
                        phone.setText(jsonObject.getString("phone"));
                    }

                    if(!jsonObject.getString("web").equals("null")){
                        web.setText(jsonObject.getString("web"));
                    }

                    InputStream stream = new ByteArrayInputStream(Base64.decode(jsonObject.getString("logo"), Base64.DEFAULT));
                    photo = BitmapFactory.decodeStream(stream);
                    logo.setImageBitmap(photo);

                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("companySettingsShow", sharedPreferences.getString("companyId", null), sharedPreferences.getString("role", null));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.contextual_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.take_photo:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, permissions, requestPermission);

                    return false;
                }else{
                    startActivityForResult(cameraIntent, 0);
                    return true;
                }

            case R.id.choose_existing:
                Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
                pickPhoto.setType("image/*");
                startActivityForResult(pickPhoto, 1);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 0){
            if(resultCode == Activity.RESULT_OK){
                photo = (Bitmap)data.getExtras().get("data");
                logo.setImageBitmap(photo);

            }
        }

        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Uri selectedImage = data.getData();
                try {
                    InputStream imageStream = this.getContentResolver().openInputStream(selectedImage);
                    photo =  BitmapFactory.decodeStream(imageStream);
                    logo.setImageBitmap(photo);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, 0);
        }
    }

    public void onSaveClicked(){

        if(companyName.getText().toString().equals("")){
            ErrorDialog.error(this, "Company name cannot be empty");
        }else{
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Saving Company Info...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] b = byteArrayOutputStream.toByteArray();
            final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    progressDialog.dismiss();
                }
            }).execute("companySettingsSet", sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null), sharedPreferences.getString("fullName", null), companyName.getText().toString(), encodedImage, sharedPreferences.getString("companyId", null), street.getText().toString(), city.getText().toString(), zip.getText().toString(), phone.getText().toString(),web.getText().toString(),state.getText().toString());

        }
    }
}
