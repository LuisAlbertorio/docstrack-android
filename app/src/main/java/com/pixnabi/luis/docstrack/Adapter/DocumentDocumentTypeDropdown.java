package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Fragments.DocumentsFragment.AddDocumentFragment;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList.DepartmentsActivity;
import com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList.DocumentTypesActivity;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Luis on 2/9/2017.
 */

public class DocumentDocumentTypeDropdown extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    JSONArray jsonArray;
    String origin;

    public DocumentDocumentTypeDropdown(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray jsonArray, String origin){
        this.jsonArray = jsonArray;
        this.origin = origin;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (jsonArray == null) ? 2 : jsonArray.length() + 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        if(position == 0){
            View rowView = inflater.inflate(R.layout.row_empty, parent, false);

            return rowView;
        }else

        if (position == getCount() - 1) {
            View rowView = inflater.inflate(R.layout.row_document_add_document_type, parent, false);

            Button addDepartment = (Button)rowView.findViewById(R.id.rowDocAddDocType);

            addDepartment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddDocumentFragment.resumeDocType = true;
                    Intent intent = new Intent(context, DocumentTypesActivity.class);
                    context.startActivity(intent);
                }
            });

            return rowView;


        } else {
            View rowView = inflater.inflate(R.layout.row_dropdowns, parent, false);

            TextView textView = (TextView) rowView.findViewById(R.id.dropdownTextView);

            try {
                textView.setText(jsonArray.getJSONObject(position-1).getString("type"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return rowView;
        }
    }
}
