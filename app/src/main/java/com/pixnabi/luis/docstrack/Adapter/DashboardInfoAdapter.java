package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Luis on 1/17/2017.
 */

public class DashboardInfoAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray data;

    public DashboardInfoAdapter(Context context){
        this.context = context;
        inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (data == null) ? 0 : data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_dashboard_doc_info, parent, false);

        TextView round = (TextView)rowView.findViewById(R.id.rowDocInfoRoundTextView);
        ListView roundInfoListView = (ListView)rowView.findViewById(R.id.rowDocInfoListView);

        DashboardRoundInfoAdapter adapter = new DashboardRoundInfoAdapter(context);
        roundInfoListView.setAdapter(adapter);

        try {
            round.setText("Workflow " + data.getJSONObject(position).getString("roundNumber"));
            JSONArray test =  data.getJSONObject(position).getJSONArray("approvers");
            JSONArray stat = data.getJSONObject(position).getJSONArray("approverStatuses");

            for(int i =0 ; i < test.length();i++){
                Log.wtf("test " + String.valueOf(i), test.getString(i));
            }

//            String approvers = data.getJSONObject(position).getString("approvers");
//            approvers = approvers.replace("[", "").replace("]","").replace("\"","");
//            String[] approverString = approvers.split(",");
//            String approverStatus = data.getJSONObject(position).getString("approverStatuses");
//            approverStatus = approverStatus.replace("[", "").replace("]","").replace("\"","");
//            String[] approverStatusArray = approverStatus.split(",");
//
//            Log.wtf("approverLen", String.valueOf(approverString.length));
//
            adapter.setData(test, stat);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rowView;
    }
}
