package com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.DocumentTypesAdapter;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class DocumentTypesActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ListView docList;
    Button add;
    DocumentTypesAdapter adapter;
    public static JSONArray data;
    ProgressBar progressBar;
    public static TextView emptyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_document_types);

        docList = (ListView)findViewById(R.id.docTypesListView);
        add = (Button)findViewById(R.id.docTypesAddButton);
        progressBar = (ProgressBar)findViewById(R.id.docTypeProgressDialog);
        emptyList = (TextView)findViewById(R.id.docTypesEmptyTextView);

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        adapter = new DocumentTypesAdapter(this);

        docList.setAdapter(adapter);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddButtonPressed();
            }
        });

        data = new JSONArray();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    Object json = new JSONTokener(result).nextValue();
                    if(json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);
                        progressBar.setVisibility(View.INVISIBLE);
                        emptyList.setText(jsonObject.getString("empty"));
                        emptyList.setVisibility(View.VISIBLE);
                    }else{
                        JSONArray array = new JSONArray(result);
                        data = array;
                        adapter.setData(array);
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println(result);

            }
        }).execute("docTypesShow", sharedPreferences.getString("role", null), sharedPreferences.getString("companyId", null));
    }

    public void onAddButtonPressed(){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_dept_doc_noti);

        TextView title, instructions;
        Button add, cancel;
        final EditText nameDialog;

        title = (TextView)dialog.findViewById(R.id.dialogAddTitleTextView);
        instructions = (TextView)dialog.findViewById(R.id.dialogAddInstruction);
        add = (Button)dialog.findViewById(R.id.dialogAddAddButton);
        cancel = (Button)dialog.findViewById(R.id.dialogAddCancelButton);
        nameDialog = (EditText)dialog.findViewById(R.id.dialogAddEditText);

        dialog.show();

        title.setText("Document Type");
        instructions.setText("Enter name of Document Type");

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(!nameDialog.equals("")){
                final ProgressDialog progressDialog = new ProgressDialog(DocumentTypesActivity.this);
                progressDialog.setMessage("Adding Document Type...");
                progressDialog.show();
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                    System.out.println(result);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        JSONObject resultJSON = new JSONObject(result);

                        if(resultJSON.has("error")){
                            progressDialog.dismiss();
                            dialog.dismiss();
                            ErrorDialog.error(DocumentTypesActivity.this, resultJSON.getString("error"));
                        }else{
                            jsonObject.put("type", nameDialog.getText().toString());
                            data.put(jsonObject);
                            adapter.setData(data);
                            progressDialog.dismiss();
                            dialog.dismiss();
                            emptyList.setVisibility(View.INVISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    }
                }).execute("docTypesCreate", sharedPreferences.getString("role", null),sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), nameDialog.getText().toString());
            }else{
                Toast.makeText(DocumentTypesActivity.this, "Document Type Name cannot be empty", Toast.LENGTH_LONG).show();
            }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
