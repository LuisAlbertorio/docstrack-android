package com.pixnabi.luis.docstrack.Adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList.NotificationsActivity;
import com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement.EditUserActivity;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Luis on 11/23/2016.
 */

public class NotificationsAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray data;
    SharedPreferences sharedPreferences;
    String oldNotiTime;

    public NotificationsAdapter(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (data == null) ? 0 : data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_departments, parent, false);

        final TextView name = (TextView) rowView.findViewById(R.id.rowDepartmentsTextView);

        try {
            name.setText(data.getJSONObject(position).getString("time") + " days");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MainActivity.sharedPreferences.getString("role", null).equals("Owner")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_edit_delete);
                    dialog.show();

                    TextView edit, delete;

                    edit = (TextView) dialog.findViewById(R.id.dialogEditDelEditButton);
                    delete = (TextView) dialog.findViewById(R.id.dialogEditDelDelButton);

                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dialog.setContentView(R.layout.dialog_edit_custom_list);

                            TextView typeText, instructionsText;
                            final EditText editText;
                            Button cancelButton, editButton;

                            typeText = (TextView) dialog.findViewById(R.id.dialogEditTitleTextView);
                            instructionsText = (TextView) dialog.findViewById(R.id.dialogEditInstruction);
                            editText = (EditText) dialog.findViewById(R.id.dialogEditEditText);
                            editButton = (Button) dialog.findViewById(R.id.dialogEditEditButton);
                            cancelButton = (Button) dialog.findViewById(R.id.dialogEditCancelButton);

                            editText.setInputType(InputType.TYPE_CLASS_NUMBER);

                            dialog.show();

                            typeText.setText("Notification");
                            instructionsText.setText("Edit Notification time");

                            try {
                                oldNotiTime = data.getJSONObject(position).getString("time");
                                editText.setText(data.getJSONObject(position).getString("time"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            editButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final ProgressDialog progressDialog = new ProgressDialog(context);
                                    progressDialog.setMessage("Editing Notification...");
                                    progressDialog.show();

                                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                        @Override
                                        public void processFinish(String result) {

                                            try {
                                                JSONObject jsonobj = new JSONObject(result);
                                                if (jsonobj.has("error")) {
                                                    ErrorDialog.error(context, jsonobj.getString("error"));
                                                    progressDialog.dismiss();

                                                } else {
                                                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                                        @Override
                                                        public void processFinish(String result) {
                                                            JSONArray jsonArray = null;
                                                            try {
                                                                jsonArray = new JSONArray(result);
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                            NotificationsActivity.data = jsonArray;
                                                            setData(jsonArray);

                                                            progressDialog.dismiss();
                                                            dialog.dismiss();
                                                        }
                                                    }).execute("notificationsShow", sharedPreferences.getString("role", null), sharedPreferences.getString("companyId", null));
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).execute("notificationsEdit", oldNotiTime, editText.getText().toString(), MainActivity.sharedPreferences.getString("companyId", null), MainActivity.sharedPreferences.getString("role", null), MainActivity.sharedPreferences.getString("fullName", null), MainActivity.sharedPreferences.getString("userId", null));

                                }
                            });

                            cancelButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });


                        }
                    });

                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dialog.setContentView(R.layout.dialog_delete_dept_doc_noti);

                            TextView title, instructions;
                            Button deleteButton, cancel;

                            title = (TextView) dialog.findViewById(R.id.dialogDeleteTitleTextView);
                            instructions = (TextView) dialog.findViewById(R.id.dialogDeleteInstruction);
                            deleteButton = (Button) dialog.findViewById(R.id.dialogDeleteDeleteButton);
                            cancel = (Button) dialog.findViewById(R.id.dialogDeleteCancelButton);

                            dialog.show();

                            title.setText("Notifications");
                            instructions.setText("Delete " + name.getText().toString() + " notification?");

                            deleteButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final ProgressDialog progressDialog = new ProgressDialog(context);
                                    progressDialog.setMessage("Deleting Notification...");
                                    progressDialog.show();
                                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                        @Override
                                        public void processFinish(String result) {
                                            System.out.println(result);

                                            try {
                                                JSONObject jsonObject = new JSONObject(result);
                                                if (jsonObject.has("error")) {
                                                    ErrorDialog.error(context, jsonObject.getString("error"));
                                                    progressDialog.dismiss();
                                                    dialog.dismiss();
                                                } else {
                                                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                                        @Override
                                                        public void processFinish(String result) {
                                                            try {
                                                                Object json = new JSONTokener(result).nextValue();

                                                                if (json instanceof JSONObject) {
                                                                    JSONObject jsonObject1 = new JSONObject(result);
                                                                    NotificationsActivity.emptyList.setText(jsonObject1.getString("empty"));
                                                                    NotificationsActivity.emptyList.setVisibility(View.VISIBLE);

                                                                    JSONArray jsonArray = new JSONArray();
                                                                    NotificationsActivity.data = jsonArray;
                                                                    setData(jsonArray);

                                                                    progressDialog.dismiss();
                                                                    dialog.dismiss();

                                                                } else {
                                                                    JSONArray jsonArray = new JSONArray(result);
                                                                    NotificationsActivity.data = jsonArray;
                                                                    setData(jsonArray);

                                                                    progressDialog.dismiss();
                                                                    dialog.dismiss();
                                                                }


                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }).execute("notificationsShow", sharedPreferences.getString("role", null), sharedPreferences.getString("companyId", null));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).execute("notificationsDelete", sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), name.getText().toString().replace(" days", ""));
                                }
                            });

                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                        }
                    });
                } else {
                    Toast.makeText(context, "You don't have permission to delete Notifications", Toast.LENGTH_LONG).show();
                }
            }
        });

        return rowView;
    }
}
