package com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.UserManagementUsersAdapter;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;

public class UserManagementActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
//    Button delete;
    FloatingActionButton add;
//    EditText email;
    ListView users;
    UserManagementUsersAdapter adapter;
    boolean onResumeBool = false;

    String companyId;
    String role;

    @Override
    protected void onResume() {
        super.onResume();

        if(onResumeBool){
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Getting users...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    System.out.println(result);
                    progressDialog.dismiss();
                    try {

                        JSONArray jsonArray = new JSONArray(result);
                        adapter.setData(jsonArray, sharedPreferences.getString("userId", null));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).execute("userManagement", companyId, role, sharedPreferences.getString("userId", null));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        add = (FloatingActionButton) findViewById(R.id.userManagementAdd);
//        delete = (Button)findViewById(R.id.userManagementDelete);
//        email = (EditText)findViewById(R.id.userManagementEditText);
        users = (ListView) findViewById(R.id.userManagementList);
        adapter = new UserManagementUsersAdapter(this);

        users.setAdapter(adapter);

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        companyId = sharedPreferences.getString("companyId", null);
        role = sharedPreferences.getString("role", null);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Getting users...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                System.out.println(result);
                progressDialog.dismiss();
                try {

                    JSONArray jsonArray = new JSONArray(result);

                    for(int i=0;i<jsonArray.length();i++){
                        Log.d("jsonArray", String.valueOf(jsonArray.getJSONObject(i)));
                    }

                    adapter.setData(jsonArray, sharedPreferences.getString("userId", null));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).execute("userManagement", companyId, role, sharedPreferences.getString("userId", null));

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserManagementActivity.this, AddUserActivity.class);
                startActivity(intent);
            }
        });

//        delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onDeleteClicked();
//            }
//        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        onResumeBool = true;
    }

//    public void onDeleteClicked(){
//        final Dialog dialog = new Dialog(this);
//        dialog.setContentView(R.layout.dialog_user_management);
//        dialog.show();
//        dialog.setCancelable(false);
//
//        final EditText emailEditText = (EditText)dialog.findViewById(R.id.userManagementDialogEditText);
//        Button cancelButton = (Button)dialog.findViewById(R.id.userManagementDialogCancelButton);
//        Button deleteButton = (Button)dialog.findViewById(R.id.userManagementDialogDeleteButton);
//
//
//        cancelButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(!emailEditText.getText().toString().equals("")){
//
//                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
//                    @Override
//                    public void processFinish(String result) {
//                        System.out.println(result);
//                        adapter.notifyDataSetChanged();
//                        dialog.dismiss();
//
//                     }
//                }).execute("userDelete", emailEditText.getText().toString(), sharedPreferences.getString("companyId",null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null));
//
//                }else{
//                    Toast.makeText(UserManagementActivity.this, "Enter an email", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//    }

//    public void onDeleteClicked(){
//
//        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
//            @Override
//            public void processFinish(String result) {
//                System.out.println(result);
//            }
//        }).execute("userDelete", email.getText().toString(), sharedPreferences.getString("companyId",null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null));
//    }
}
