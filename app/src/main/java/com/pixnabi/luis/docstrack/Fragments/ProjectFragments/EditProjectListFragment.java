package com.pixnabi.luis.docstrack.Fragments.ProjectFragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pixnabi.luis.docstrack.Adapter.EditProjectListAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProjectListFragment extends Fragment {

    SharedPreferences sharedPreferences;

    public EditProjectListFragment() {
        // Required empty public constructor
    }

    ListView listView;
    EditProjectListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_project_list, container, false);

        listView = (ListView)rootView.findViewById(R.id.editProjectListView);
        adapter = new EditProjectListAdapter(getActivity());

        MainActivity.titleName.setText("Edit Project");

        listView.setAdapter(adapter);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        MainActivity.progressBar.setVisibility(View.VISIBLE);

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                System.out.println(result);

                try {
                    JSONArray jsonArray = new JSONArray(result);
                    adapter.setData(jsonArray);
                    MainActivity.progressBar.setVisibility(View.INVISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("editProjectShowProjects", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null));

        return rootView;
    }


//    public void finalEdit(String proId, String proName){
//        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
//            @Override
//            public void processFinish(String result) {
//                System.out.println(result);
//            }
//        }).execute("editProjectSet", sharedPreferences.getString("userId", null), "2025/05/25", sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null),proId, proName);
//    }

}
