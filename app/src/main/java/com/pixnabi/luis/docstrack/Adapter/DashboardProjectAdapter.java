package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.media.Image;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Luis on 11/29/2016.
 */

public class DashboardProjectAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray jsonArray;
    MainActivity mainActivity;

    public DashboardProjectAdapter(Context context){
        this.context = context;
        mainActivity = (MainActivity)context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray jsonArray){
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (jsonArray == null) ? 0 : jsonArray.length() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(position == jsonArray.length()){
            View rowView = inflater.inflate(R.layout.row_dashboard_create_project, parent, false);

            Button create = (Button)rowView.findViewById(R.id.rowDashboardCreateProjectButton);
            if(mainActivity.sharedPreferences.getString("role", null).equals("Viewer") || mainActivity.sharedPreferences.getString("role", null).equals("Approver")){
                create.setVisibility(View.GONE);
            }

            create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.NewProject("dashboard");
                }
            });

            return rowView;
        }else{

            View rowView = inflater.inflate(R.layout.row_dashboard_projects, parent, false);

            TextView title, owner, dueDate, name;
            ImageView status;

            title = (TextView)rowView.findViewById(R.id.rowDashboardProjectsTitle);
            owner = (TextView)rowView.findViewById(R.id.rowDashboardProjectsOwner);
            dueDate = (TextView)rowView.findViewById(R.id.rowDashboardProjectsDuedate);
            status = (ImageView)rowView.findViewById(R.id.rowDashboardStatusCircle);
            name = (TextView)rowView.findViewById(R.id.rowDashboardProjectsName);

            try {
                title.setText(jsonArray.getJSONObject(position).getString("number"));
                owner.setText(jsonArray.getJSONObject(position).getString("owner"));
                dueDate.setText(jsonArray.getJSONObject(position).getString("dueDate"));
                name.setText(jsonArray.getJSONObject(position).getString("name"));

            if(jsonArray.getJSONObject(position).getString("status").equals("Approved")){
                status.setImageResource(R.drawable.green_blank_circle);
            }else if(jsonArray.getJSONObject(position).getString("status").equals("In Review")){
                status.setImageResource(R.drawable.yellow_blank_circle);
            }else if(jsonArray.getJSONObject(position).getString("status").equals("Rejected") || jsonArray.getJSONObject(position).getString("status").equals("Late")){
                status.setImageResource(R.drawable.red_blank_circle);
            }else if(jsonArray.getJSONObject(position).getString("status").equals("Awaiting Start")){
                status.setImageResource(R.drawable.gray_blank_circle);
            }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        mainActivity.GotoDocumentDashboard(jsonArray.getJSONObject(position).getString("id"), jsonArray.getJSONObject(position).getString("name"), jsonArray.getJSONObject(position).getString("status"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return rowView;
        }
    }
}
