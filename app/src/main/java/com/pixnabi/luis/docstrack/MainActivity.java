package com.pixnabi.luis.docstrack;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.Fragments.DashboardFragments.DashboardDocumentFragment;
import com.pixnabi.luis.docstrack.Fragments.DashboardFragments.DocumentInfoFragment;
import com.pixnabi.luis.docstrack.Fragments.DocumentsFragment.AddDocumentFragment;
import com.pixnabi.luis.docstrack.Fragments.DocumentsFragment.EditDocumentFragment;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.AddProjectFragment;
import com.pixnabi.luis.docstrack.Fragments.DashboardFragments.DashboardFragment;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.AddProjectFromDocumentFragment;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.EditProjectFragment;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.EditProjectListFragment;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.ProjectCreatedConfirmationFragment;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.ProjectsFragment;
import com.pixnabi.luis.docstrack.Fragments.ReportsFragments.ReportsFragment;
import com.pixnabi.luis.docstrack.Fragments.WorkflowFragments.WorkflowAdminOwnerFragment;
import com.pixnabi.luis.docstrack.Fragments.WorkflowFragments.WorkflowApproverFragment;
import com.pixnabi.luis.docstrack.Fragments.WorkflowFragments.WorkflowConfirmationFragment;
import com.pixnabi.luis.docstrack.Fragments.WorkflowFragments.WorkflowFragment;
import com.pixnabi.luis.docstrack.SideMenuActivities.CompanySettings.CompanySettingsActivity;
import com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList.SystemCustomListActivity;
import com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement.UserManagementActivity;
import com.pixnabi.luis.docstrack.Utility.FileDownloader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    public static SharedPreferences sharedPreferences;
    ImageButton drawerMenu, popupMenu;
    DrawerLayout drawerLayout;
    public static TextView titleName;
    FrameLayout frameLayout;
    FragmentTransaction fragmentTransaction;
    public static ProgressBar progressBar;
    TextView navDrawerName;
    CircleImageView profileImageView;
    Bitmap photo = null;
    boolean resume = false;

    DashboardFragment dashboardFragment;
    ProjectsFragment projectsFragment;
    WorkflowFragment workflowFragment;
    ReportsFragment reportsFragment;
    DashboardDocumentFragment dashboardDocumentFragment;
    AddProjectFragment addProjectFragment;
    AddDocumentFragment addDocumentFragment;

    @Override
    protected void onResume() {
        super.onResume();

        if (resume) {
            InputStream stream = new ByteArrayInputStream(Base64.decode(sharedPreferences.getString("photo", null), Base64.DEFAULT));
            photo = BitmapFactory.decodeStream(stream);
            profileImageView.setImageBitmap(photo);
        }
    }

    //TODO: Falta confirmation screen de approve or reject

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        dashboardFragment = new DashboardFragment();
        projectsFragment = new ProjectsFragment();
        workflowFragment = new WorkflowFragment();
        reportsFragment = new ReportsFragment();

        drawerMenu = (ImageButton) findViewById(R.id.menuButton);
        popupMenu = (ImageButton) findViewById(R.id.popupButton);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        titleName = (TextView) findViewById(R.id.titleTextView);
        frameLayout = (FrameLayout) findViewById(R.id.mainFrameLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBarMain);
        navDrawerName = (TextView) findViewById(R.id.navDrawerProfileName);
        profileImageView = (CircleImageView) findViewById(R.id.navDrawerProfilePicture);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, dashboardFragment);
        fragmentTransaction.commit();

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        navDrawerName.setText(sharedPreferences.getString("fullName", null));

        InputStream stream = new ByteArrayInputStream(Base64.decode(sharedPreferences.getString("photo", null), Base64.DEFAULT));
        photo = BitmapFactory.decodeStream(stream);
        profileImageView.setImageBitmap(photo);

        drawerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
            }
        });

        popupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });

    }

    public void showMenu() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void dismissMenu() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void showPopup() {

        PopupMenu popupMenu = new PopupMenu(MainActivity.this, this.popupMenu);

        if (sharedPreferences.getString("role", null).equals("Viewer") || sharedPreferences.getString("role", null).equals("Approver")) {
            popupMenu.getMenuInflater().inflate(R.menu.setup_options_viewer, popupMenu.getMenu());
        } else if (sharedPreferences.getString("role", null).equals("Owner")) {
            popupMenu.getMenuInflater().inflate(R.menu.setup_options_owner, popupMenu.getMenu());
        } else {
            popupMenu.getMenuInflater().inflate(R.menu.setup_options, popupMenu.getMenu());
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.user_management) {
                    Intent intent = new Intent(MainActivity.this, UserManagementActivity.class);
                    startActivity(intent);
                }

                if (item.getItemId() == R.id.system_custom_lists) {
                    Intent intent = new Intent(MainActivity.this, SystemCustomListActivity.class);
                    startActivity(intent);
                }

                if (item.getItemId() == R.id.company_settings) {
                    Intent intent = new Intent(MainActivity.this, CompanySettingsActivity.class);
                    startActivity(intent);
                }

                if (item.getItemId() == R.id.log_out) {

                    final Dialog dialog = new Dialog(MainActivity.this);
                    dialog.setContentView(R.layout.dialog_exit);
                    dialog.show();

                    Button cancelButton = (Button) dialog.findViewById(R.id.dialogExitCancelButton);
                    Button exitButton = (Button) dialog.findViewById(R.id.dialogExitExitButton);

                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    exitButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.commit();

                            Intent intent = new Intent(MainActivity.this, LogInActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });


                }

                return true;
            }
        });

//        setForceShowIcon(popupMenu);

        popupMenu.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (sharedPreferences.getString("role", null).equals("Viewer")) {
            inflater.inflate(R.menu.setup_options_viewer, menu);
        } else {
            inflater.inflate(R.menu.setup_options, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    public void goToUserProfile(View view) {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
        resume = true;
    }

    public void LaunchMenuItems(View view) {
        switch (view.getId()) {

            case R.id.navDrawerDashboardButton:

                if (!dashboardFragment.isVisible()) {

                    frameLayout.removeAllViews();
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.mainFrameLayout, dashboardFragment);
                    fragmentTransaction.commit();

//                    titleName.setText("Dashboard");

                    dismissMenu();
                    break;

                } else {
                    dismissMenu();
                    break;
                }


            case R.id.navDrawerProjectsButton:

                if (!projectsFragment.isVisible()) {
                    if (sharedPreferences.getString("role", null).equals("Viewer") || sharedPreferences.getString("role", null).equals("Approver")) {
                        Toast.makeText(this, "You don't have permission to access that area", Toast.LENGTH_LONG).show();
                        dismissMenu();
                    } else {
                        frameLayout.removeAllViews();
                        fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.mainFrameLayout, projectsFragment);
                        fragmentTransaction.commit();

//                    titleName.setText("Documents");

                        dismissMenu();
                        break;
                    }
                } else {
                    dismissMenu();
                    break;
                }


            case R.id.navDrawerWorkflowButton:

                if (!workflowFragment.isVisible()) {
                    if (sharedPreferences.getString("role", null).equals("Viewer")) {
                        Toast.makeText(this, "You don't have permission to access that area", Toast.LENGTH_LONG).show();
                        dismissMenu();
                    } else {
                        frameLayout.removeAllViews();
                        fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.mainFrameLayout, workflowFragment);
                        fragmentTransaction.commit();

//                    titleName.setText("Workflow");

                        dismissMenu();
                        break;
                    }
                } else {
                    dismissMenu();
                    break;
                }


            case R.id.navDrawerReportsButton:

                if (!reportsFragment.isVisible()) {
                    frameLayout.removeAllViews();
                    fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.mainFrameLayout, reportsFragment);
                    fragmentTransaction.commit();

                    dismissMenu();
                    break;
                } else {
                    dismissMenu();
                    break;
                }

            case R.id.navDrawerAboutButton:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;

        }
    }

    /**
     * Called from EditProjectListFragment
     */
    public void EditProject() {
        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, new EditProjectListFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
//        titleName.setText("Edit Project");
    }

    /**
     * Called from AddProjectFragment
     */
    public void NewProject(String origin) {
        addProjectFragment = new AddProjectFragment();

        Bundle bundle = new Bundle();
        bundle.putString("origin", origin);

        addProjectFragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, addProjectFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
//        titleName.setText("Create Project");
    }

    public void reloadWorkflow() {

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, workflowFragment);
        fragmentTransaction.commit();

        titleName.setText("Workflow");
    }

    public void ConfirmProjectCreation(String name, String origin) {

        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("origin", origin);

        ProjectCreatedConfirmationFragment fragment = new ProjectCreatedConfirmationFragment();

        fragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.commit();
    }

    public void createProjectFromDocument(){

        AddProjectFromDocumentFragment fragment = new AddProjectFromDocumentFragment();

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void confirmCreatedProjectFromDocument(String proName){

        ProjectCreatedConfirmationFragment fragment = new ProjectCreatedConfirmationFragment();

        Bundle bundle = new Bundle();
        bundle.putString("name", proName);

        fragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.commit();
    }

    public void backToCurrentDocument(){
        getFragmentManager().popBackStackImmediate();
    }

    public void BackToProjects(String origin) {

        if (origin.equals("normal")) {
            for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                getFragmentManager().popBackStack();
            }
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.remove(projectsFragment);
            fragmentTransaction.remove(dashboardFragment);

            projectsFragment = new ProjectsFragment();

            frameLayout.removeAllViews();
            fragmentTransaction.replace(R.id.mainFrameLayout, projectsFragment);
            fragmentTransaction.commit();
        } else if (origin.equals("dashboard")) {
            for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                getFragmentManager().popBackStack();
            }

            Log.d("LOG Error", "Dashboard add");

            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.remove(dashboardFragment);
            frameLayout.removeAllViews();

            dashboardFragment = new DashboardFragment();

            fragmentTransaction.replace(R.id.mainFrameLayout, dashboardFragment);
            fragmentTransaction.commit();
        }
    }

    public void EditProjectInfo(String id) {
        EditProjectFragment fragment = new EditProjectFragment();

        Bundle bundle = new Bundle();
        bundle.putString("id", id);

        fragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void CreateDocument(String origin) {

        addDocumentFragment = new AddDocumentFragment();

        Bundle bundle = new Bundle();
        bundle.putString("origin", origin);

        addDocumentFragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, addDocumentFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void EditDocument() {
        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, new EditDocumentFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void GotoDocumentDashboard(String projectId, String name, String status) {
        dashboardDocumentFragment = new DashboardDocumentFragment();
        Bundle bundle = new Bundle();
        bundle.putString("projectId", projectId);
        bundle.putString("projectName", name);
        bundle.putString("status", status);

        dashboardDocumentFragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, dashboardDocumentFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void showDocumentInfo(String docId, String docName, String owner) {
        DocumentInfoFragment fragment = new DocumentInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("docId", docId);
        bundle.putString("docName", docName);
        bundle.putString("docOwner", owner);

        fragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void workflowAdminorOwner(String docId, String docName, String docNum) {
        WorkflowAdminOwnerFragment fragment = new WorkflowAdminOwnerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("docName", docName);
        bundle.putString("docId", docId);
        bundle.putString("docNum", docNum);

        fragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void workflowApprover(String docId, String docName, String docNum) {
        WorkflowApproverFragment fragment = new WorkflowApproverFragment();
        Bundle bundle = new Bundle();
        bundle.putString("docName", docName);
        bundle.putString("docId", docId);
        bundle.putString("docNum", docNum);

        fragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void createDocumentFromDashboard() {

        addDocumentFragment = new AddDocumentFragment();

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, addDocumentFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

//        titleName.setText("Add Document");

    }

    public void createProjectFromDashboard(String origin) {

        addProjectFragment = new AddProjectFragment();

        Bundle bundle = new Bundle();
        bundle.putString("origin", origin);

        addProjectFragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, addProjectFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

//        persistentTitleCheck();
    }

    public void workflowConfirmationScreen(String action, String docName) {

        WorkflowConfirmationFragment workflowConfirmationFragment;
        workflowConfirmationFragment = new WorkflowConfirmationFragment();

        Bundle bundle = new Bundle();
        bundle.putString("action", action);
        bundle.putString("name", docName);
        workflowConfirmationFragment.setArguments(bundle);

        frameLayout.removeAllViews();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, workflowConfirmationFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void backToWorkflow() {
        fragmentTransaction = getFragmentManager().beginTransaction();
        for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
            getFragmentManager().popBackStack();
        }

        fragmentTransaction.remove(dashboardFragment);

        frameLayout.removeAllViews();
        fragmentTransaction.replace(R.id.mainFrameLayout, workflowFragment);
        fragmentTransaction.commit();
    }

    public void goToReportsWebView(String url) {

        Intent intent = new Intent(this, ReportWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {

        if (projectsFragment.isVisible()) {

            for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                getFragmentManager().popBackStack();
            }

            frameLayout.removeAllViews();
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.remove(dashboardFragment);
            dashboardFragment = new DashboardFragment();
            fragmentTransaction.replace(R.id.mainFrameLayout, dashboardFragment);
            fragmentTransaction.commit();

//            titleName.setText("Dashboard");

//            persistentTitleCheck();
        } else if (workflowFragment.isVisible()) {
            for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                getFragmentManager().popBackStack();
            }

//            dashboardFragment = new DashboardFragment();

            frameLayout.removeAllViews();
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.remove(dashboardFragment);
            dashboardFragment = new DashboardFragment();
            fragmentTransaction.replace(R.id.mainFrameLayout, dashboardFragment);
            fragmentTransaction.commit();

//            persistentTitleCheck();
//            titleName.setText("Dashboard");

        } else if (reportsFragment.isVisible()) {
            for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                getFragmentManager().popBackStack();
            }

            frameLayout.removeAllViews();
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.remove(dashboardFragment);
            dashboardFragment = new DashboardFragment();
            fragmentTransaction.replace(R.id.mainFrameLayout, dashboardFragment);
            fragmentTransaction.commit();

//            persistentTitleCheck();
//            titleName.setText("Dashboard");

        } else if (dashboardFragment.isVisible()) {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_exit);
            dialog.show();

            Button cancelButton = (Button) dialog.findViewById(R.id.dialogExitCancelButton);
            Button exitButton = (Button) dialog.findViewById(R.id.dialogExitExitButton);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            exitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.commit();

                    Intent intent = new Intent(MainActivity.this, LogInActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

        } else {
            getFragmentManager().popBackStack();
        }
    }

//    // usually, subclasses of AsyncTask are declared inside the activity class.
//// that way, you can easily modify the UI thread from here
//    private class DownloadTask extends AsyncTask<String, Integer, String> {
//
//        private Context context;
//        private PowerManager.WakeLock mWakeLock;
//
//        public DownloadTask(Context context) {
//            this.context = context;
//        }
//
//        @Override
//        protected String doInBackground(String... sUrl) {
//            InputStream input = null;
//            OutputStream output = null;
//            HttpURLConnection connection = null;
//            try {
//                URL url = new URL(sUrl[0]);
//                connection = (HttpURLConnection) url.openConnection();
//                connection.connect();
//
//                // expect HTTP 200 OK, so we don't mistakenly save error report
//                // instead of the file
//                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                    return "Server returned HTTP " + connection.getResponseCode()
//                            + " " + connection.getResponseMessage();
//                }
//
//                // this will be useful to display download percentage
//                // might be -1: server did not report the length
//                int fileLength = connection.getContentLength();
//
//                // download the file
//                input = connection.getInputStream();
//                output = new FileOutputStream("/sdcard/doc.pdf");
//
//                byte data[] = new byte[4096];
//                long total = 0;
//                int count;
//                while ((count = input.read(data)) != -1) {
//                    // allow canceling with back button
//                    if (isCancelled()) {
//                        input.close();
//                        return null;
//                    }
//                    total += count;
//                    // publishing the progress....
//                    if (fileLength > 0) // only if total length is known
//                        publishProgress((int) (total * 100 / fileLength));
//                    output.write(data, 0, count);
//                }
//            } catch (Exception e) {
//                return e.toString();
//            } finally {
//                try {
//                    if (output != null)
//                        output.close();
//                    if (input != null)
//                        input.close();
//                } catch (IOException ignored) {
//                }
//
//                if (connection != null)
//                    connection.disconnect();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
////            mWakeLock.release();
//            if (result != null)
//                Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();
//            else
//                Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
//        }
//    }

    private class DownloadFile extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf

//            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
//            File folder = new File(extStorageDirectory, "testthreepdf");
//            folder.mkdir();

            File mydir = getDir("mydir", Context.MODE_PRIVATE);

            File pdfFile = new File(mydir, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            view();
            super.onPostExecute(aVoid);
        }
    }

    public void view()
    {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/mydir/" + "maven.pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try{
            startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(MainActivity.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }
}
