package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Luis on 11/30/2016.
 */

public class EditProjectListAdapter extends BaseAdapter{

    Context context;
    LayoutInflater inflater;
    JSONArray jsonArray;
    MainActivity mainActivity;

    public EditProjectListAdapter(Context context){
        this.context = context;
        mainActivity = (MainActivity)context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray jsonArray){
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (jsonArray == null) ? 0 : jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_editprojects_list_items, parent, false);

        TextView name = (TextView)rowView.findViewById(R.id.editProjectListItemTextView);
        TextView owner = (TextView)rowView.findViewById(R.id.editProjectListItemOwner);
        TextView number = (TextView)rowView.findViewById(R.id.editProjectListItemProNumber);
        TextView dueDate = (TextView)rowView.findViewById(R.id.editProjectListItemDueDate);

        try {
            name.setText(jsonArray.getJSONObject(position).getString("name"));
            owner.setText(jsonArray.getJSONObject(position).getString("owner"));
            number.setText(jsonArray.getJSONObject(position).getString("num"));
            dueDate.setText(jsonArray.getJSONObject(position).getString("dueDate"));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        mainActivity.EditProjectInfo(jsonArray.getJSONObject(position).getString("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }



        return rowView;
    }
}
