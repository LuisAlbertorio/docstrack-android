package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.ImageToString;

import org.json.JSONArray;
import org.json.JSONException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Luis on 12/21/2016.
 */

public class WorkflowAdminOwnerApproversAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray data;

    public WorkflowAdminOwnerApproversAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (data == null) ? 0 : data.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_workflow_approve_behalf, parent, false);
        TextView name = (TextView)rowView.findViewById(R.id.rowApproverBehalfName);
        CircleImageView circleImageView = (CircleImageView)rowView.findViewById(R.id.rowApproveBehalfImage);

        try {
            name.setText(data.getJSONObject(position).getString("aFullName"));
            circleImageView.setImageBitmap(ImageToString.StringToImage(data.getJSONObject(position).getString("aProfilePicture")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rowView;
    }
}
