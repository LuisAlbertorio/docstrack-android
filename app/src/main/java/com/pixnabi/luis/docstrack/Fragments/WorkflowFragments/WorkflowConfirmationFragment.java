package com.pixnabi.luis.docstrack.Fragments.WorkflowFragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkflowConfirmationFragment extends Fragment {


    public WorkflowConfirmationFragment() {
        // Required empty public constructor
    }

    String action, docName, reason;

    TextView name, reasonTextView;
    ImageView imageView;
    Button backMenu;

    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_workflow_confirmation, container, false);
        name = (TextView)rootview.findViewById(R.id.workflowConfirmationNameTextView);
        reasonTextView = (TextView)rootview.findViewById(R.id.workflowConfirmationReasonReject);
        imageView = (ImageView)rootview.findViewById(R.id.workflowConfirmationImageView);
        backMenu = (Button)rootview.findViewById(R.id.workflowConfirmationButton);

        MainActivity.titleName.setText("Workflow");
        mainActivity = (MainActivity)getActivity();

        action = getArguments().getString("action");
        docName = getArguments().getString("name");

        if(action.equals("reject")){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.rejectworkflow));
            name.setText(docName + " was rejected");
            reasonTextView.setText(reason);
        }else if(action.equals("approve")){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.approveworkflow));
            name.setText(docName + " was successfully approved");
        }else{
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.stopworkflow));
            name.setText(docName + " workflow was stoppped");
        }

        backMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.backToWorkflow();
            }
        });

        return rootview;
    }

}
