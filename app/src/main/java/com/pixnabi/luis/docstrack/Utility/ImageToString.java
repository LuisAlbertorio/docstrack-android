package com.pixnabi.luis.docstrack.Utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by Luis on 1/9/2017.
 */

public class ImageToString {

    public static String ImageToString(Bitmap photo){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encodedImage;
    }

    public static Bitmap StringToImage(String image){
        InputStream stream = new ByteArrayInputStream(Base64.decode(image, Base64.DEFAULT));
        Bitmap photo = BitmapFactory.decodeStream(stream);

        return photo;
    }

}
