package com.pixnabi.luis.docstrack.Fragments.WorkflowFragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkflowApproverFragment extends Fragment {


    public WorkflowApproverFragment() {
        // Required empty public constructor
    }

    ImageButton approve, reject;
    EditText reason;
    TextView name;

    SharedPreferences sharedPreferences;
    String docId;

    MainActivity mainActivity;

    ProgressDialog progressDialog;

    String docNum;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_workflow_approver, container, false);

        mainActivity = (MainActivity) getActivity();

        approve = (ImageButton) rootView.findViewById(R.id.workflowApproverApproveButton);
        reject = (ImageButton) rootView.findViewById(R.id.workflowApproverRejectButton);
        reason = (EditText) rootView.findViewById(R.id.workflowApproverEditText);
        name = (TextView) rootView.findViewById(R.id.workflowApproverNameTextView);
        MainActivity.titleName.setText("Workflow");

        progressDialog = new ProgressDialog(getActivity());

        name.setText(getArguments().getString("docName"));

        docNum = getArguments().getString("docNum");

        docId = getArguments().getString("docId");

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovedClicked();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRejectClicked();
            }
        });

        return rootView;
    }

    public void onApprovedClicked() {
        progressDialog.setMessage("Approving...");
        progressDialog.show();
        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    final JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.has("project_now_approved")){
                        JSONArray pushId = jsonObject.getJSONArray("project_now_approved");
                        String pushNotiString = "";

                        for(int i=0;i<pushId.length();i++){
                            if(pushNotiString.equals("")){
                                if(!pushId.getString(i).equals("null")) {
                                    pushNotiString = "'" + pushId.getString(i) + "'";
                                }
                            }else{
                                if(!pushId.getString(i).equals("null")) {
                                    pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                                }
                            }
                        }

                        final String finalPushNotiString = pushNotiString;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String pushString = "The document with ID " + docNum + " was approved and closed. Thank you all for your help during this process.";
                                try {
                                    OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                            new OneSignal.PostNotificationResponseHandler() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                }

                                                @Override
                                                public void onFailure(JSONObject response) {
                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                }
                                            });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();

                        final String projectId = jsonObject.getString("id");
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String pushString = "All the documents of the project ID " + projectId + " were approved and closed. Thank you all for your help during this process.";
                                try {
                                    OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                            new OneSignal.PostNotificationResponseHandler() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                }

                                                @Override
                                                public void onFailure(JSONObject response) {
                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                }
                                            });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                    if(jsonObject.has("document_now_approved")){
                        JSONArray pushId = jsonObject.getJSONArray("document_now_approved");
                        String pushNotiString = "";

                        for(int i=0;i<pushId.length();i++){
                            if(pushNotiString.equals("")){
                                pushNotiString = "'" + pushId.getString(i) + "'";
                            }else{
                                pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                            }
                        }

                        final String finalPushNotiString = pushNotiString;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String pushString = "The document with ID " + docNum + " was approved and closed . Thank you all for your help during this process.";
                                try {
                                    OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                            new OneSignal.PostNotificationResponseHandler() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                }

                                                @Override
                                                public void onFailure(JSONObject response) {
                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                }
                                            });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();

                    }
                    else if (jsonObject.has("success")) {
                        if (!jsonObject.getString("success").equals("null")) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String pushString = "Document ID " + docNum + " was approved by " + sharedPreferences.getString("department", null) + ". You will receive the document in any moment for your approval.";
                                    try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': ['" + jsonObject.getString("success") + "'], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                new OneSignal.PostNotificationResponseHandler() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                    }

                                                    @Override
                                                    public void onFailure(JSONObject response) {
                                                        Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    }
                    progressDialog.dismiss();
                    mainActivity.workflowConfirmationScreen("approve", name.getText().toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("WorkflowSet", sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), docId, "Approve", "", "");
    }

    public void onRejectClicked() {
        if (!reason.getText().toString().equals("")) {
            progressDialog.setMessage("Rejecting...");
            progressDialog.show();
            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("success")) {
                        JSONArray pushId = jsonObject.getJSONArray("success");
                        String pushNotiString = "";

                        for (int i = 0; i < pushId.length(); i++) {
                            if (pushNotiString.equals("")) {
                                pushNotiString = "'" + pushId.getString(i) + "'";
                            } else {
                                pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                            }
                        }

                        Log.wtf("pushNoti", pushNotiString);

                        final String finalPushNotiString = pushNotiString;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                            String pushString = "Document ID " + docNum + " was rejected by " + sharedPreferences.getString("department", null) + ". A new approval process will start shortly.";
                            try {
                                OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                    new OneSignal.PostNotificationResponseHandler() {
                                        @Override
                                        public void onSuccess(JSONObject response) {
                                            Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                        }

                                        @Override
                                        public void onFailure(JSONObject response) {
                                            Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                        }
                                    });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            }
                        }).start();
                    }

                    progressDialog.dismiss();
                    mainActivity.workflowConfirmationScreen("reject", name.getText().toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }
            }).execute("WorkflowSet", sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), docId, "Reject", reason.getText().toString(), "");
        } else {
            Toast.makeText(getActivity(), "You must add a reason", Toast.LENGTH_LONG).show();
        }
    }
}
