package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Luis on 12/20/2016.
 */

public class DashboardDocumentAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    MainActivity mainActivity;

    JSONArray data;

    public DashboardDocumentAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainActivity = (MainActivity)context;
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (data == null){
            return 0;
        }else if(data.length() == 0){
            return data.length();
        }else{
            return data.length() + 1;
        }

//        return (data == null) ? 0 : data.length() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(position == data.length()){
            View rowView = inflater.inflate(R.layout.row_dashboard_create_project, parent, false);

            Button create = (Button)rowView.findViewById(R.id.rowDashboardCreateProjectButton);

            create.setText("Create Document");

            if(mainActivity.sharedPreferences.getString("role", null).equals("Viewer") || mainActivity.sharedPreferences.getString("role", null).equals("Approver")){
                create.setVisibility(View.GONE);
            }

            create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.CreateDocument("dashboard");
                }
            });

            return rowView;

        }else{

            View rowView = inflater.inflate(R.layout.row_dashboard_projects, parent, false);

            final TextView title, owner, dueDate, name;
            ImageView status;

            title = (TextView)rowView.findViewById(R.id.rowDashboardProjectsTitle);
            owner = (TextView)rowView.findViewById(R.id.rowDashboardProjectsOwner);
            dueDate = (TextView)rowView.findViewById(R.id.rowDashboardProjectsDuedate);
            status = (ImageView)rowView.findViewById(R.id.rowDashboardStatusCircle);
            name = (TextView)rowView.findViewById(R.id.rowDashboardProjectsName);

            name.setVisibility(View.GONE);

            try {
                title.setText(data.getJSONObject(position).getString("num"));
                owner.setText(data.getJSONObject(position).getString("owner"));
                dueDate.setText(data.getJSONObject(position).getString("dueDate"));

                if(data.getJSONObject(position).getString("status").equals("Approved")){
                    status.setImageResource(R.drawable.green_blank_circle);
                }else if(data.getJSONObject(position).getString("status").equals("In Review")){
                    status.setImageResource(R.drawable.yellow_blank_circle);
                }else if(data.getJSONObject(position).getString("status").equals("Rejected") || data.getJSONObject(position).getString("status").equals("Late") || data.getJSONObject(position).getString("status").equals("Stopped")){
                    status.setImageResource(R.drawable.red_blank_circle);
                }else if(data.getJSONObject(position).getString("status").equals("Awaiting Start")){
                    status.setImageResource(R.drawable.gray_blank_circle);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Log.wtf("STATUS", data.getJSONObject(position).getString("status"));

//                        if(!data.getJSONObject(position).getString("status").equals("Awaiting Start")){
                            mainActivity.showDocumentInfo(data.getJSONObject(position).getString("id"), title.getText().toString(), owner.getText().toString());
//                        }else{
//                            Toast.makeText(context, "Document is not in review", Toast.LENGTH_LONG).show();
//                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return rowView;
        }
    }
}
