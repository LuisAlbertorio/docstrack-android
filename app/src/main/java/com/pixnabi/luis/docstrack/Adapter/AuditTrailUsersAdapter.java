package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Luis on 2/16/2017.
 */

public class AuditTrailUsersAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray data;

    public AuditTrailUsersAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (data == null) ? 0 : data.length() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_dropdowns, parent, false);

        TextView textView = (TextView)rowView.findViewById(R.id.dropdownTextView);


        if(position == 0){
            textView.setText("All");
        }else{
            try {
                textView.setText(data.getJSONObject(position-1).getString("uFullName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return rowView;
    }
}
