package com.pixnabi.luis.docstrack.Utility;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Luis on 11/9/2016.
 */

public class BackgroundWorker extends AsyncTask<String, Void, String> {

    Context context;
    AlertDialog alertDialog;
    public AsyncResponse delegate = null;

    public BackgroundWorker(AsyncResponse delegate) {
        this.delegate = delegate;
    }


    String baseUrl = "http://www.arrozcongandules.com/";

    @Override
    protected String doInBackground(String... params) {
        String type = params[0];

        /**
         * Login, SignUp, ForgotPass Files
         */
        if(type.equals("login")) {
            String login_url = baseUrl+"logIn.php";
            try {
                String email = params[1];
                String password = params[2];
                String push_id= params[3];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(password, "UTF-8") +"&"+
                        URLEncoder.encode("push_id", "UTF-8")+"="+URLEncoder.encode(push_id, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("signUp")){
            String login_url = baseUrl+"signUp.php";
            try {
                String email = params[1];
                String phone = params[2];
                String firstName = params[3];
                String lastName = params[4];
                String companyName = params[5];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8")+"&"+
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8") + "&" +
                        URLEncoder.encode("first_name", "UTF-8")+"="+URLEncoder.encode(firstName, "UTF-8") + "&" +
                        URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(lastName, "UTF-8") + "&" +
                        URLEncoder.encode("company_name", "UTF-8")+"="+URLEncoder.encode(companyName, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("signUpExisting")){
            String login_url = baseUrl+"signUpExisting.php";
            try {
                String email = params[1];
                String phone = params[2];
                String firstName = params[3];
                String lastName = params[4];
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8")+"&"+
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8") + "&" +
                        URLEncoder.encode("first_name", "UTF-8")+"="+URLEncoder.encode(firstName, "UTF-8") + "&" +
                        URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(lastName, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("forgotPassVerifyEmail")){
            String login_url = baseUrl+"forgotPasswordEmailVerify.php";
               try {
                String email = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("forgotPassVerifyQuestions")){
            String login_url = baseUrl+"forgotPasswordQuestionsVerify.php";
                    try {
                String question1 = params[1];
                String question2 = params[2];
                String email = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("answer_one", "UTF-8")+"="+URLEncoder.encode(question1, "UTF-8")+"&"+
                        URLEncoder.encode("answer_two", "UTF-8")+"="+URLEncoder.encode(question2, "UTF-8") + "&"+
                        URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8") ;
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /**
         * //////////////////////////////////////////////////////////
         */


        /**
         * User Profile Files
         */
        if(type.equals("firstTimeShow")){
            String login_url = baseUrl + "firstTimeProfileSetupShow.php";
            try {
                String tokenid = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("token_id", "UTF-8")+"="+URLEncoder.encode(tokenid, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("firstTimeSet")){
            String login_url = baseUrl + "firstTimeProfileSetupSet.php";
            try {
                String token = params[1];
                String pass = params[2];
                String quesOne = params[3];
                String ansOne = params[4];
                String quesTwo = params[5];
                String ansTwo = params[6];
                String photo = params[7];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("token_id", "UTF-8")+"="+URLEncoder.encode(token, "UTF-8") + "&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(pass, "UTF-8") + "&"+
                        URLEncoder.encode("security_question_one", "UTF-8")+"="+URLEncoder.encode(quesOne, "UTF-8") + "&"+
                        URLEncoder.encode("answer_one", "UTF-8")+"="+URLEncoder.encode(ansOne, "UTF-8") + "&"+
                        URLEncoder.encode("security_question_two", "UTF-8")+"="+URLEncoder.encode(quesTwo, "UTF-8") + "&"+
                        URLEncoder.encode("answer_two", "UTF-8")+"="+URLEncoder.encode(ansTwo, "UTF-8") + "&" +
                        URLEncoder.encode("profile_picture", "UTF-8")+"="+URLEncoder.encode(photo, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("profileShow")){
            String login_url = baseUrl + "profileShow.php";
            try {
                String tokenid = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("token_id", "UTF-8")+"="+URLEncoder.encode(tokenid, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("profileSet")){
            String login_url = baseUrl + "profileSet.php";
            try {

                String tokenid = params[1];
                String quesOne = params[2];
                String ansOne = params[3];
                String quesTwo = params[4];
                String ansTwo = params[5];
                String photo = params[6];
                String phone = params[7];
                String dept = params[8];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("token_id", "UTF-8")+"="+URLEncoder.encode(tokenid, "UTF-8") + "& " +
                        URLEncoder.encode("security_question_one", "UTF-8")+"="+URLEncoder.encode(quesOne, "UTF-8") + "&"+
                        URLEncoder.encode("answer_one", "UTF-8")+"="+URLEncoder.encode(ansOne, "UTF-8") + "&"+
                        URLEncoder.encode("security_question_two", "UTF-8")+"="+URLEncoder.encode(quesTwo, "UTF-8") + "&"+
                        URLEncoder.encode("answer_two", "UTF-8")+"="+URLEncoder.encode(ansTwo, "UTF-8") + "&" +
                        URLEncoder.encode("profile_picture", "UTF-8")+"="+URLEncoder.encode(photo, "UTF-8")+ "&" +
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8")+ "&" +
                        URLEncoder.encode("department", "UTF-8")+"="+URLEncoder.encode(dept, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * //////////////////////////////////////////////////////////
         */


        /**
         * Side Menu Files
         */
        if(type.equals("userManagement")){
            String login_url = baseUrl + "userSetupShow.php";
            try {
                String companyId = params[1];
                String role = params[2];
                String userId = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("userCreate")){
            String login_url = baseUrl + "userSetupCreate.php";
             try {
                String first = params[1];
                String last = params[2];
                String phone = params[3];
                String email = params[4];
                String role = params[5];
                String fullName = params[6];
                String loggedRole = params[7];
                String companyId = params[8];
                String userId = params[9];
                String department = params[10];

                 System.out.println("EMAIL: " + email);

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("first_name", "UTF-8")+"="+URLEncoder.encode(first, "UTF-8") + "& " +
                        URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(last, "UTF-8") + "& " +
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8") + "& " +
                        URLEncoder.encode("new_email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8") + "& " +
                        URLEncoder.encode("role", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(loggedRole, "UTF-8")+ "& " +
                        URLEncoder.encode("department", "UTF-8")+"="+URLEncoder.encode(department, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("userDelete")){
            String login_url = baseUrl + "userSetupDelete.php";
            try {
                String email = params[1];
                String companyId = params[2];
                String fullName = params[3];
                String role = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("userEditShow")){
            String login_url = baseUrl + "userSetupEditShow.php";
            try {
                String companyId = params[1];
                String role = params[2];
                String userId = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("userEditSet")){
            String login_url = baseUrl + "userSetupEditSet.php";
            try {
                String userId = params[1];
                String companyId = params[2];
                String id = params[3];
                String role = params[4];
                String fname = params[5];
                String lname = params[6];
                String phone = params[7];
                String department = params[8];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("fname", "UTF-8")+"="+URLEncoder.encode(fname, "UTF-8") + "& " +
                        URLEncoder.encode("lname", "UTF-8")+"="+URLEncoder.encode(lname, "UTF-8") + "& " +
                        URLEncoder.encode("user_edit_id", "UTF-8")+"="+URLEncoder.encode(id, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8")+ "& " +
                        URLEncoder.encode("department", "UTF-8")+"="+URLEncoder.encode(department, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("userEditPassRefresh")){
            String login_url = baseUrl + "userSetupPasswordRefresh.php";
            try {
                String email = params[1];
                String companyId = params[2];
                String userId = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("departmentShow")){
            String login_url = baseUrl + "departmentSetupShow.php";
            try {
                String role = params[1];
                String companyId = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("departmentCreate")){
            String login_url = baseUrl + "departmentSetupCreate.php";
            try {
                String role = params[1];
                String userId = params[2];
                String companyId = params[3];
                String deptname = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("department_name", "UTF-8")+"="+URLEncoder.encode(deptname, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("departmentDelete")){
            String login_url = baseUrl + "departmentSetupDelete.php";
            try {
                String role = params[1];
                String userId = params[2];
                String companyId = params[3];
                String deptname = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("department_name", "UTF-8")+"="+URLEncoder.encode(deptname, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("departmentEdit")){
            String login_url = baseUrl + "departmentSetupEdit.php";
            try {
                String old_department_name = params[1];
                String new_department_name = params[2];
                String companyId = params[3];
                String role = params[4];
                String fullName = params[5];
                String userId = params[6];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("old_department_name", "UTF-8")+"="+URLEncoder.encode(old_department_name, "UTF-8") + "& " +
                        URLEncoder.encode("new_department_name", "UTF-8")+"="+URLEncoder.encode(new_department_name, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8")+ "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8")+ "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("docTypesShow")){
            String login_url = baseUrl + "documentTypeSetupShow.php";
            try {
                String role = params[1];
                String companyId = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("docTypesCreate")){
            String login_url = baseUrl + "documentTypeSetupCreate.php";
            try {
                String role = params[1];
                String userId = params[2];
                String companyId = params[3];
                String docname = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("document_type_name", "UTF-8")+"="+URLEncoder.encode(docname, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("docTypesDelete")){
            String login_url = baseUrl + "documentTypeSetupDelete.php";
            try {
                String role = params[1];
                String userId = params[2];
                String companyId = params[3];
                String docname = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("document_type_name", "UTF-8")+"="+URLEncoder.encode(docname, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("docTypesEdit")){
            String login_url = baseUrl + "documentTypeSetupEdit.php";
            try {
                String old_document_type_name = params[1];
                String new_document_type_name = params[2];
                String companyId = params[3];
                String role = params[4];
                String fullName = params[5];
                String userId = params[6];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("old_document_type_name", "UTF-8")+"="+URLEncoder.encode(old_document_type_name, "UTF-8") + "& " +
                        URLEncoder.encode("new_document_type_name", "UTF-8")+"="+URLEncoder.encode(new_document_type_name, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8")+ "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8")+ "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("notificationsShow")){
            String login_url = baseUrl + "notificationSetupShow.php";
            try {
                String role = params[1];
                String companyId = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("notificationsCreate")){
            String login_url = baseUrl + "notificationSetupCreate.php";
            try {
                String role = params[1];
                String userId = params[2];
                String companyId = params[3];
                String notiTime = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("notification_time", "UTF-8")+"="+URLEncoder.encode(notiTime, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("notificationsDelete")){
            String login_url = baseUrl + "notificationSetupDelete.php";
            try {
                String role = params[1];
                String userId = params[2];
                String companyId = params[3];
                String notiTime = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("notification_time", "UTF-8")+"="+URLEncoder.encode(notiTime, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("notificationsEdit")){
            String login_url = baseUrl + "notificationSetupEdit.php";
            try {
                String old_notification_time = params[1];
                String new_notification_time = params[2];
                String companyId = params[3];
                String role = params[4];
                String fullName = params[5];
                String userId = params[6];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data = URLEncoder.encode("old_notification_time", "UTF-8")+"="+URLEncoder.encode(old_notification_time, "UTF-8") + "& " +
                        URLEncoder.encode("new_notification_time", "UTF-8")+"="+URLEncoder.encode(new_notification_time, "UTF-8") + "& " +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "& " +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8")+ "& " +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8")+ "& " +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("companySettingsShow")){
            String login_url = baseUrl + "companySettingsShow.php";
            try {
                String companyId = params[1];
                String role = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("companySettingsSet")){
            String login_url = baseUrl + "companySettingsSet.php";
            try {
                String role = params[1];
                String user_id = params[2];
                String full_name = params[3];
                String company_name = params[4];
                String company_logo = params[5];
                String company_id = params[6];
                String address_street = params[7];
                String address_city = params[8];
                String address_zip = params[9];
                String company_phone = params[10];
                String company_web_address = params[11];
                String address_state = params[12];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("company_name", "UTF-8")+"="+URLEncoder.encode(company_name, "UTF-8") + "&" +
                        URLEncoder.encode("company_logo", "UTF-8")+"="+URLEncoder.encode(company_logo, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8")  + "&" +
                        URLEncoder.encode("address_street", "UTF-8")+"="+URLEncoder.encode(address_street, "UTF-8")  + "&" +
                        URLEncoder.encode("address_city", "UTF-8")+"="+URLEncoder.encode(address_city, "UTF-8")  + "&" +
                        URLEncoder.encode("address_zip", "UTF-8")+"="+URLEncoder.encode(address_zip, "UTF-8")  + "&" +
                        URLEncoder.encode("company_phone", "UTF-8")+"="+URLEncoder.encode(company_phone, "UTF-8")  + "&" +
                        URLEncoder.encode("company_web_address", "UTF-8")+"="+URLEncoder.encode(company_web_address, "UTF-8")   + "&" +
                        URLEncoder.encode("address_state", "UTF-8")+"="+URLEncoder.encode(address_state, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /**
         * //////////////////////////////////////////////////////////
         */


        /**
         * Dashboard Files
         */
        if(type.equals("dashboardShow")){
            String login_url = baseUrl + "dashboardProjectShow.php";
            try {
                String role_session = params[1];
                String user_id = params[2];
                String full_name = params[3];
                String company_id = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("dashboardDocumentShow")){
            String login_url = baseUrl + "dashboardDocumentsShow.php";
            try {
                String project_id = params[1];
                String role_session = params[2];
                String user_id = params[3];
                String full_name = params[4];
                String company_id = params[5];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("project_id", "UTF-8")+"="+URLEncoder.encode(project_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("dashboardDocumentInfoShow")){
            String login_url = baseUrl + "dashboardDocumentInfoShow.php";
            try {
                String company_id = params[1];
                String document_id= params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("document_id", "UTF-8")+"="+URLEncoder.encode(document_id, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        /**
         * //////////////////////////////////////////////////////////
         */

        /**
         * Projects Files
         */
        if(type.equals("projectCreate")){
            String login_url = baseUrl + "addDocumentsProjectCreate.php";
            try {
                String project_name = params[1];
                String project_number = params[2];
                String due_date = params[3];
                String owner = params[4];
                String user_id = params[5];
                String company_id = params[6];
                String role_session = params[7];
                String full_name = params[8];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("project_name", "UTF-8")+"="+URLEncoder.encode(project_name, "UTF-8") + "&" +
                        URLEncoder.encode("project_number", "UTF-8")+"="+URLEncoder.encode(project_number, "UTF-8") + "&" +
                        URLEncoder.encode("due_date", "UTF-8")+"="+URLEncoder.encode(due_date, "UTF-8") + "&" +
                        URLEncoder.encode("owner", "UTF-8")+"="+URLEncoder.encode(owner, "UTF-8") + "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("editProjectShowProjects")){
            String login_url = baseUrl + "addDocumentsProjectEditShow.php";
            try {
                String company_id = params[1];
                String full_name = params[2];
                String role_session = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("editProjectShowProjectInfo")){
            String login_url = baseUrl + "addDocumentsProjectEditInfoShow.php";
            try {
                String company_id = params[1];
                String project_id = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                                URLEncoder.encode("project_id", "UTF-8")+"="+URLEncoder.encode(project_id, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("editProjectSet")){
            String login_url = baseUrl + "addDocumentsProjectEditInfoSet.php";
            try {
                String user_id = params[1];
                String due_date = params[2];
                String full_name = params[3];
                String company_id = params[4];
                String project_id = params[5];
                String project_name = params[6];
                String project_number = params[7];

                Log.wtf("id, name, number = ", project_id + ", " + project_name + ", " + project_number);

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                        URLEncoder.encode("due_date", "UTF-8")+"="+URLEncoder.encode(due_date, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("la_bruja", "UTF-8")+"="+URLEncoder.encode(project_id, "UTF-8") + "&" +
                        URLEncoder.encode("proyecto_nombre", "UTF-8")+"="+URLEncoder.encode(project_name, "UTF-8")+ "&" +
                        URLEncoder.encode("project_number", "UTF-8")+"="+URLEncoder.encode(project_number, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /**
         * //////////////////////////////////////////////////////////
         */


        /**
         * Document Files
         */
        if(type.equals("DocumentCreate")){
            String login_url = baseUrl + "addDocumentsDocumentCreate.php";
            try {
                String project_id = params[1];
                String document_number = params[2];
                String document_name = params[3];
                String document_type = params[4];
                String due_date = params[5];
                String notifications = params[6];
                notifications = notifications.replace(" days", "");
                String department = params[7];
                String approval_type = params[8];
                String approver_name = params[9];
                String final_approver = params[10];
                String arraytoString = approver_name;
                String finalApproverString = final_approver;
                arraytoString = arraytoString.replace("[", "");
                arraytoString = arraytoString.replace("]", "");
                finalApproverString = finalApproverString.replace("[", "");
                finalApproverString = finalApproverString.replace("]", "");
                String user_id = params[11];
                String company_id = params[12];
                String full_name = params[13];
                String owner = params[14];
                String role_session = params[15];
                String approver_user_id = params[16];
                String approver_user_id_string = approver_user_id.replace("[", "").replace("]","");
                String owner_id = params[17];

                System.out.println("approverName: " + arraytoString);
                System.out.println("approverUserId: " + approver_user_id_string);

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("project_id", "UTF-8")+"="+URLEncoder.encode(project_id, "UTF-8") + "&" +
                                URLEncoder.encode("document_number", "UTF-8")+"="+URLEncoder.encode(document_number, "UTF-8") + "&" +
                                URLEncoder.encode("document_name", "UTF-8")+"="+URLEncoder.encode(document_name, "UTF-8") + "&" +
                                URLEncoder.encode("document_type", "UTF-8")+"="+URLEncoder.encode(document_type, "UTF-8") + "&" +
                                URLEncoder.encode("due_date", "UTF-8")+"="+URLEncoder.encode(due_date, "UTF-8") + "&" +
                                URLEncoder.encode("notification_time", "UTF-8")+"="+URLEncoder.encode(notifications, "UTF-8") + "&" +
                                URLEncoder.encode("approval_type", "UTF-8")+"="+URLEncoder.encode(approval_type, "UTF-8") + "&" +
                                URLEncoder.encode("approver_name", "UTF-8")+"="+URLEncoder.encode(arraytoString, "UTF-8") + "&" +
                                URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                                URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                                URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                                URLEncoder.encode("owner", "UTF-8")+"="+URLEncoder.encode(owner, "UTF-8") + "&" +
                                URLEncoder.encode("final_approver", "UTF-8")+"="+URLEncoder.encode(finalApproverString, "UTF-8") + "&" +
                                URLEncoder.encode("department", "UTF-8")+"="+URLEncoder.encode(department, "UTF-8") + "&" +
                                URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8")+ "&" +
                                URLEncoder.encode("approver_user_id", "UTF-8")+"="+URLEncoder.encode(approver_user_id_string, "UTF-8") + "&" +
                                URLEncoder.encode("owner_id", "UTF-8")+"="+URLEncoder.encode(owner_id, "UTF-8") ;

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DocumentInfo")){
            String login_url = baseUrl + "addDocumentsDocumentEditInfoShow.php";
            try {
                String document_id = params[1];
                String company_id = params[2];


                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("document_id", "UTF-8")+"="+URLEncoder.encode(document_id, "UTF-8") + "&" +
                                URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("EditDocument")){
            String login_url = baseUrl + "addDocumentsDocumentEditInfoSet.php";
            try {
                String document_id = params[1];
                String document_name = params[2];
                String due_date = params[3];
                String notification_time = params[4];
                String company_id = params[5];
                String full_name = params[6];
                String document_number = params[7];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("document_id", "UTF-8")+"="+URLEncoder.encode(document_id, "UTF-8") + "&" +
                                URLEncoder.encode("document_name", "UTF-8")+"="+URLEncoder.encode(document_name, "UTF-8") + "&" +
                                URLEncoder.encode("due_date", "UTF-8")+"="+URLEncoder.encode(due_date, "UTF-8") + "&" +
                                URLEncoder.encode("notification_time", "UTF-8")+"="+URLEncoder.encode(notification_time, "UTF-8") + "&" +
                                URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                                URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                                URLEncoder.encode("document_number", "UTF-8")+"="+URLEncoder.encode(document_number, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * //////////////////////////////////////////////////////////
         */

        /**
         * Dropdowns for Add Documents and Edit Documents
         */
        if(type.equals("DropdownProjects")){
            String login_url = baseUrl + "dropdownReportProjects.php";
            try {
                String companyId = params[1];
                String fullName = params[2];
                String role = params[3];
                String user_id = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DropdownDepartments")){
            String login_url = baseUrl + "dropdownDepartments.php";
            try {
                String companyId = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DropdownNotifications")){
            String login_url = baseUrl + "dropdownNotifications.php";
            try {
                String companyId = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DropdownDocumentTypes")){
            String login_url = baseUrl + "dropdownDocumentTypes.php";
            try {
                String companyId = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DropdownApprovers")){
            String login_url = baseUrl + "dropdownApprovers.php";
            try {
                String companyId = params[1];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DocEditProjectShow")){
            String login_url = baseUrl + "addDocumentsDocumentEditProjectsShow.php";
            try {
                String role_session = params[1];
                String full_name = params[2];
                String company_id = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("DocEditDocShow")){
            String login_url = baseUrl + "addDocumentsDocumentEditShow.php";
            try {
                String project_id = params[1];
                String company_id = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                                URLEncoder.encode("project_id", "UTF-8")+"="+URLEncoder.encode(project_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("dropdownOwners")){
            String login_url = baseUrl + "dropdownOwners.php";
            try {
                String company_id = params[1];
                String full_name = params[2];
                String role_session = params[3];
                String user_id = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8")+ "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("dropdownApproversByDepartment")){
            String login_url = baseUrl + "dropdownApproversByDepartment.php";
            try {
                String company_id = params[1];
                String department = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("department", "UTF-8")+"="+URLEncoder.encode(department, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        /**
         * //////////////////////////////////////////////////////////
         */

        /**
         * Workflow files
         */
        if(type.equals("WorkflowShow")){
            String login_url = baseUrl + "workflowsDocumentShow.php";
            try {
                String role_session = params[1];
                String full_name = params[2];
                String company_id = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("WorkflowSet")){
            String login_url = baseUrl + "workflowsApprovalSet.php";
            try {
                String user_id = params[1];
                String company_id = params[2];
                String full_name = params[3];
                String document_id = params[4];
                String action = params[5];
                String comment = params[6];
                String approver = params[7];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("document_id", "UTF-8")+"="+URLEncoder.encode(document_id, "UTF-8") + "&" +
                        URLEncoder.encode("action", "UTF-8")+"="+URLEncoder.encode(action, "UTF-8") + "&" +
                        URLEncoder.encode("comment", "UTF-8")+"="+URLEncoder.encode(comment, "UTF-8")+ "&" +
                        URLEncoder.encode("on_behalf_approver", "UTF-8")+"="+URLEncoder.encode(approver, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("WorkflowApproversSpinner")){
            String login_url = baseUrl + "dropdownWorkflowApprovers.php";
            try {
                String document_id = params[1];
                String company_id = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("document_id", "UTF-8")+"="+URLEncoder.encode(document_id, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * //////////////////////////////////////////////////////////
         */

        /**
         * Report Files
         */
        if(type.equals("DocumentTimeDistribution")){
            String login_url = baseUrl + "dropdownReportDocuments.php";
            try {
                String role_session= params[1];
                String full_name = params[2];
                String company_id = params[3];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("ProjectTimeDistribution")){
            String login_url = baseUrl + "dropdownReportProjects.php";
            try {
                String role_session= params[1];
                String full_name = params[2];
                String company_id = params[3];
                String user_id = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("full_name", "UTF-8")+"="+URLEncoder.encode(full_name, "UTF-8") + "&" +
                                URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                                URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8")+ "&" +
                                URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("AuditTrailUsers")){
            String login_url = baseUrl + "dropdownReportUsers.php";
            try {
                String user_id= params[1];
                String company_id = params[2];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                                URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                                URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("WorkflowDistributionReportUsers")){
            String login_url = baseUrl + "dropdownReportWorkflowUsers.php";
            try {
                String role = params[1];
                String role_session = params[2];
                String company_id = params[3];
                String user_id = params[4];

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("role", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8") + "&" +
                        URLEncoder.encode("role_session", "UTF-8")+"="+URLEncoder.encode(role_session, "UTF-8") + "&" +
                        URLEncoder.encode("company_id", "UTF-8")+"="+URLEncoder.encode(company_id, "UTF-8") + "&" +
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(type.equals("GenerateReport")){
            String login_url = baseUrl + "reportGenerator.php";
            try {
                String reportType = params[1];
                String companyId = params[2];
                String fullName = params[3];
                String projectNumber = params[4];
                String documentNumber = params[5];
                String document_type = params[6];
                String fromDate = params[7];
                String toDate = params[8];
                String userId = params[9];
                String fromDateAudit = params[10];
                String toDateAudit = params[11];
                String role = params[12];
                String name = params[13];
                String id = params[14];

                Random r = new Random(System.currentTimeMillis());
                String randomNumber= String.valueOf((1+r.nextInt(50)) * 10000 + r.nextInt(10000));
                Log.d("randomNumber", randomNumber);

                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                String post_data =
                        URLEncoder.encode("reportType", "UTF-8")+"="+URLEncoder.encode(reportType, "UTF-8") + "&" +
                        URLEncoder.encode("companyId", "UTF-8")+"="+URLEncoder.encode(companyId, "UTF-8") + "&" +
                        URLEncoder.encode("fullName", "UTF-8")+"="+URLEncoder.encode(fullName, "UTF-8") + "&" +
                        URLEncoder.encode("project_number", "UTF-8")+"="+URLEncoder.encode(projectNumber, "UTF-8")+ "&" +
                        URLEncoder.encode("document_number", "UTF-8")+"="+URLEncoder.encode(documentNumber, "UTF-8")+ "&" +
                        URLEncoder.encode("document_type", "UTF-8")+"="+URLEncoder.encode(document_type, "UTF-8")+ "&" +
                        URLEncoder.encode("fromDate", "UTF-8")+"="+URLEncoder.encode(fromDate, "UTF-8")+ "&" +
                        URLEncoder.encode("toDate", "UTF-8")+"="+URLEncoder.encode(toDate, "UTF-8")+ "&" +
                        URLEncoder.encode("userId", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8")+ "&" +
                        URLEncoder.encode("fromDateAudit", "UTF-8")+"="+URLEncoder.encode(fromDateAudit, "UTF-8")+ "&" +
                        URLEncoder.encode("toDateAudit", "UTF-8")+"="+URLEncoder.encode(toDateAudit, "UTF-8")+ "&" +
                        URLEncoder.encode("role", "UTF-8")+"="+URLEncoder.encode(role, "UTF-8")+ "&" +
                        URLEncoder.encode("name", "UTF-8")+"="+URLEncoder.encode(name, "UTF-8")+ "&" +
                        URLEncoder.encode("id", "UTF-8")+"="+URLEncoder.encode(id, "UTF-8")+ "&" +
                        URLEncoder.encode("randomNumber", "UTF-8")+"="+URLEncoder.encode(randomNumber, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(String result) {

        System.out.println("RESULT: " + result);

        delegate.processFinish(result);

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    public interface AsyncResponse{
        void processFinish(String result);
    }
}
