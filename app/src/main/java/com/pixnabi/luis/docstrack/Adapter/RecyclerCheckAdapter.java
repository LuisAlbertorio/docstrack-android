package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.R;

import java.util.ArrayList;

/**
 * Created by Luis on 12/16/2016.
 */

public class RecyclerCheckAdapter  extends RecyclerView.Adapter<RecyclerCheckAdapter.VH>{

    Context context;
    ArrayList<String> info;
    boolean[] checkBoxBool;
    CheckBox lastCheckedBox = null;
    int lastCheckedPos = 1000;
    ArrayList<String> namePlusDepartment;

    public RecyclerCheckAdapter(Context context){
        this.context = context;
    }

    public void setData(ArrayList<String> info, ArrayList<String> namePlusDepartment){
        this.info = info;
        this.namePlusDepartment = namePlusDepartment;
        checkBoxBool = new boolean[info.size()];
        notifyDataSetChanged();
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycle_check, parent, false);
        return new VH(itemView);
    }

    @Override
    public void onBindViewHolder(final VH holder, final int position) {
        holder.check.setText(info.get(position));

        final int tempPos = position;

        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox)v;
                int clickedPos = tempPos;
                Log.wtf("HELP", String.valueOf(clickedPos));

                if(lastCheckedBox != null){
                    lastCheckedBox.setChecked(false);
                    lastCheckedBox = cb;
                    lastCheckedPos = clickedPos;
                }else{
                    lastCheckedBox = cb;
                    lastCheckedPos = clickedPos;
                }
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFromList(position);
            }
        });
    }

    public void removeFromList(int pos){
        if(info.size() == 1){
            info.remove(0);
            namePlusDepartment.remove(0);
        }else{
            info.remove(pos);
            namePlusDepartment.remove(pos);
        }
        notifyItemRemoved(pos);
    }

    public int returnInteger(){
        return lastCheckedPos;
    }

    @Override
    public int getItemCount() {
        return (info == null) ? 0 : info.size();
    }

    public static class VH extends RecyclerView.ViewHolder {

        protected CheckBox check;
        protected ImageButton remove;

        public VH(View itemView) {
            super(itemView);
            check = (CheckBox)itemView.findViewById(R.id.recycleCheckbox);
            remove = (ImageButton)itemView.findViewById(R.id.recycleRemoveButton);
        }
    }
}
