package com.pixnabi.luis.docstrack;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Adapter.SecurityQuestionsAdapter;
import com.pixnabi.luis.docstrack.Adapter.SpinnerAdapter;
import com.pixnabi.luis.docstrack.Adapter.UserProfileDepartmentDropdown;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity {

    Spinner securityQuestionOne, securityQuestionTwo;
    SecurityQuestionsAdapter adapter;
    SharedPreferences sharedPreferences;

    TextView name, role, email;
    Button confirm;
    EditText answerOne, answerTwo, phone;

    ImageButton addPictureButton;

    String token;

    String[] permissions = {Manifest.permission.CAMERA};
    int requestPermission = 3;

    Bitmap photo = null;

    CircleImageView profileImageView;

    Spinner department;

    JSONArray deptData;
    String userDept;

    UserProfileDepartmentDropdown userProfileDepartmentDropdown;

    public static boolean resumeDept;

    ProgressDialog progressDialog;

    Bitmap newPhoto;

    public String[] questions = {"Who was your childhood hero?",
            "What was the name of the company where you had your first job?",
            "What time of the day were you born?(hh:mm)",
            "In what city or town did your parents meet?",
            "What is the name of your favorite childhood friend?",
            "In what town or city was your first full-time job?",
            "What was the make and model of your first car?",
            "What was your favorite sport in high school?",
            "What school did you attend for sixth grade?"};


    @Override
    protected void onResume() {
        super.onResume();
        if(resumeDept){
            resumeDept = false;

            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        deptData = jsonArray;
                        userProfileDepartmentDropdown.setData(jsonArray, "departments");

                        for(int i =0; i <jsonArray.length();i++){
                            if(jsonArray.getJSONObject(i).getString("name").equals(userDept)){
                                department.setSelection(i+1);
                            }
                        }

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile2);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_user_profile2);

        name = (TextView)findViewById(R.id.userProfileRegNameTextView);
        role = (TextView)findViewById(R.id.userProfileRegRoleTextView);
        email = (TextView)findViewById(R.id.userProfileRegEmailTextView);
        phone = (EditText) findViewById(R.id.userProfileRegPhoneEditText);
        confirm = (Button)findViewById(R.id.userProfileRegConfirmButton);
        securityQuestionOne = (Spinner)findViewById(R.id.userProfileRegSecurityQuestionSpinnerOne);
        securityQuestionTwo = (Spinner)findViewById(R.id.userProfileRegSecurityQuestionSpinnerTwo);
        answerOne = (EditText)findViewById(R.id.userProfileRegAnswerOne);
        answerTwo = (EditText)findViewById(R.id.userProfileRegAnswerTwo);
        addPictureButton = (ImageButton)findViewById(R.id.userProfileRegAddPhotoImageButton);
        profileImageView = (CircleImageView)findViewById(R.id.userProfileRegImageView);
        department = (Spinner) findViewById(R.id.userProfileDepartmentSpinner);

        answerOne.clearFocus();
        answerTwo.clearFocus();

        deptData = new JSONArray();
        userProfileDepartmentDropdown = new UserProfileDepartmentDropdown(this);
        department.setAdapter(userProfileDepartmentDropdown);

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        InputStream stream = new ByteArrayInputStream(Base64.decode(sharedPreferences.getString("photo", null), Base64.DEFAULT));
        photo = BitmapFactory.decodeStream(stream);
        profileImageView.setImageBitmap(photo);

        token = sharedPreferences.getString("tokenId", null);

        Log.d("TOKEN", token);

        adapter = new SecurityQuestionsAdapter(this);

        registerForContextMenu(addPictureButton);

        addPictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContextMenu(v);
            }
        });

        securityQuestionOne.setAdapter(adapter);
        securityQuestionTwo.setAdapter(adapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Getting profile info..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
            System.out.println(result);

            try {
                JSONObject jsonObject = new JSONObject(result);

                name.setText(sharedPreferences.getString("fullName", null));
                role.setText(jsonObject.getString("pRole"));
                email.setText(jsonObject.getString("pEmail"));
                phone.setText(jsonObject.getString("pPhone"));
                answerOne.setText(jsonObject.getString("pAnswerOne"));
                answerTwo.setText(jsonObject.getString("pAnswerTwo"));
                userDept = jsonObject.getString("pDepartment");

                for(int i=0; i<questions.length;i++){
                    if(jsonObject.getString("pQuestionOne").equals(questions[i])){
                        securityQuestionOne.setSelection(i);
                    }
                }

                for(int i=0; i<questions.length;i++){
                    if(jsonObject.getString("pQuestionTwo").equals(questions[i])){
                        securityQuestionTwo.setSelection(i);
                    }
                }

                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            deptData = jsonArray;
                            userProfileDepartmentDropdown.setData(jsonArray, "departments");

                            for(int i =0; i <jsonArray.length();i++){
                                if(jsonArray.getJSONObject(i).getString("name").equals(userDept)){
                                    department.setSelection(i+1);
                                }
                            }

                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            }
        }).execute("profileShow", token);


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmClicked();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.contextual_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.take_photo:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivityForResult(intent, 1);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, permissions, requestPermission);

                    return false;
                }else{
                    startActivityForResult(cameraIntent, 0);
                    return true;
                }

            case R.id.choose_existing:
                Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
                pickPhoto.setType("image/*");
                startActivityForResult(pickPhoto, 1);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    public int getOrientation(Activity activity, Uri selectedImage) {
        int orientation = 0;
        final String[] projection = new String[]{MediaStore.Images.Media.ORIENTATION};
        final Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
        if(cursor != null) {
            final int orientationColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
            if(cursor.moveToFirst()) {
                orientation = cursor.isNull(orientationColumnIndex) ? 0 : cursor.getInt(orientationColumnIndex);
            }
            cursor.close();
        }
        return orientation;
    }

    @Override
    public void grantUriPermission(String toPackage, Uri uri, int modeFlags) {
        super.grantUriPermission(toPackage, uri, modeFlags);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 0){
            if(resultCode == Activity.RESULT_OK){
                photo = (Bitmap)data.getExtras().get("data");
                Uri hello = data.getData();
                Log.d("URI START", String.valueOf(hello));

                int orientation = getOrientation(this, hello);
                Matrix matrix = new Matrix();

                if(orientation == 90){
                    matrix.postRotate(90);
                }else if(orientation == 180){
                    matrix.postRotate(180);
                }else if(orientation == 270){
                    matrix.postRotate(270);
                }

                InputStream imageStream = null;
                try {
                    imageStream = this.getContentResolver().openInputStream(hello);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                photo =  BitmapFactory.decodeStream(imageStream);

                newPhoto = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);

                profileImageView.setImageBitmap(newPhoto);
            }
        }

        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Uri selectedImage = data.getData();
                try {

                    int orientation = getOrientation(this, selectedImage);

                    Matrix matrix = new Matrix();

                    if(orientation == 90){
                        matrix.postRotate(90);
                    }else if(orientation == 180){
                        matrix.postRotate(180);
                    }else if(orientation == 270){
                        matrix.postRotate(270);
                    }


                    InputStream imageStream = this.getContentResolver().openInputStream(selectedImage);
                    photo =  BitmapFactory.decodeStream(imageStream);

                    newPhoto = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);

                    profileImageView.setImageBitmap(newPhoto);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, 0);
        }
    }

    public void onConfirmClicked(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Saving User Profile...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String questionOne = questions[securityQuestionOne.getSelectedItemPosition()];
        String questionTwo = questions[securityQuestionTwo.getSelectedItemPosition()];
        String ans1 = answerOne.getText().toString();
        String ans2 = answerTwo.getText().toString();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        newPhoto.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] b = byteArrayOutputStream.toByteArray();
        final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

//        Log.wtf("SENT INFO: " , "QOne: " + questionOne + ", QTwo: " + questionTwo + ", AnsOne: " + ans1 + ", AnsTwo: " + ans2);

        try {
            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {

                    try {

                        Object json = new JSONTokener(result).nextValue();

                        if(json instanceof JSONObject){
                            JSONObject jsonObject = new JSONObject(result);
                            if(jsonObject.has("error")){
                                ErrorDialog.error(UserProfileActivity.this, jsonObject.getString("error"));
                                progressDialog.dismiss();
                            }else{

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("photo", encodedImage);
                                editor.apply();
                                progressDialog.dismiss();
                                finish();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).execute("profileSet", token, questionOne, ans1, questionTwo, ans2, encodedImage, phone.getText().toString(), deptData.getJSONObject(department.getSelectedItemPosition()-1).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
