package com.pixnabi.luis.docstrack;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;
import com.pixnabi.luis.docstrack.Utility.ImageToString;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


public class SignUpActivity extends AppCompatActivity {

    EditText email, phone, firstName, lastName, companyName;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        email = (EditText)findViewById(R.id.signUpEmailEditText);
        phone = (EditText)findViewById(R.id.signUpPhoneEditText);
        firstName = (EditText)findViewById(R.id.signUpFirstNameEditText);
        lastName = (EditText)findViewById(R.id.signUpLastNameEditText);
        companyName = (EditText)findViewById(R.id.signUpCompanyNameEditText);
        submit = (Button)findViewById(R.id.signUpSubmitButton);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmitPressed();
            }
        });
    }

    public void onSubmitPressed(){
        final String emailStr = email.getText().toString();
        final String phoneStr = phone.getText().toString();
        final String firstNameStr = firstName.getText().toString();
        final String lastNameStr = lastName.getText().toString();
        String companyNameStr = companyName.getText().toString();

        if(email.getText().toString().equals("") || phone.getText().toString().equals("") || firstName.getText().toString().equals("") || lastName.getText().toString().equals("") || companyName.getText().toString().equals("")){
            Toast.makeText(this, "All fields must be completed", Toast.LENGTH_LONG).show();
        }else{
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Signing up..");
            progressDialog.show();

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    System.out.println("RESULT IN PROCESS: " + result);
                    progressDialog.dismiss();

                    try {

                        Object json = new JSONTokener(result).nextValue();

                        if (result.equals("Sign up was successful.")){
                            Intent intent = new Intent(SignUpActivity.this, SignUpThanksActivity.class);
                            startActivity(intent);
                            LogInActivity.activityObj.finish();
                            finish();
                        }else if(json instanceof JSONObject){


                            JSONObject jsonObject = new JSONObject(result);

                            if(jsonObject.has("result")){
                                ErrorDialog.error(SignUpActivity.this, jsonObject.getString("result"));
                            }else{
                                final Dialog dialog = new Dialog(SignUpActivity.this);
                                dialog.setContentView(R.layout.dialog_signup);

                                Button finishSignup, cancel;
                                TextView infoText;
                                ImageView companyLogo;

                                finishSignup = (Button)dialog.findViewById(R.id.dialogSignupSignupButton);
                                cancel = (Button)dialog.findViewById(R.id.dialogSignupCancelButton);
                                infoText = (TextView)dialog.findViewById(R.id.dialogSignupTextView);
                                companyLogo = (ImageView)dialog.findViewById(R.id.dialogSignupCompanyLogo);

                                companyLogo.setImageBitmap(ImageToString.StringToImage(jsonObject.getString("cLogo")));
                                infoText.setText("As defined by your email a company by the name of "+ jsonObject.getString("cName") +" already exist. Do you want to create an admintrator account in this company?");

                                dialog.show();

                                finishSignup.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        progressDialog.setMessage("Signing up...");
                                        progressDialog.show();

                                        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                            @Override
                                            public void processFinish(String result) {
                                                try {
                                                    Object json = new JSONTokener(result).nextValue();
                                                    if(json instanceof JSONObject){
                                                        JSONObject jsonObject1 = new JSONObject(result);
                                                        if(jsonObject1.has("success")){
                                                            Intent intent = new Intent(SignUpActivity.this, SignUpThanksActivity.class);
                                                            startActivity(intent);
                                                            progressDialog.dismiss();
                                                            LogInActivity.activityObj.finish();
                                                            finish();
                                                        }
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }).execute("signUpExisting", emailStr, phoneStr, firstNameStr, lastNameStr);
                                    }
                                });

                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });

                            }



                        }else{
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                System.out.println(String.valueOf(jsonObject));
                                Toast.makeText(SignUpActivity.this, jsonObject.getString("result"), Toast.LENGTH_LONG).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }
            }).execute("signUp", emailStr, phoneStr, firstNameStr, lastNameStr, companyNameStr);

        }


    }
}

