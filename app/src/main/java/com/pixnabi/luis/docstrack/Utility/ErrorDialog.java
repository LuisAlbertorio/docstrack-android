package com.pixnabi.luis.docstrack.Utility;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

/**
 * Created by Luis on 1/10/2017.
 */

public class ErrorDialog {


    public ErrorDialog(){
    }

    public static void error(Context context, String errorString){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_error);

        TextView error = (TextView)dialog.findViewById(R.id.dialogErrorTextView);
        Button okButton = (Button)dialog.findViewById(R.id.dialogErrorButton);
        dialog.setCancelable(false);
        dialog.show();


        error.setText(errorString);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });




    }

}
