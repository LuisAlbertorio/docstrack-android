package com.pixnabi.luis.docstrack.Fragments.ProjectFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectCreatedFromDocumentFragment extends Fragment {

    MainActivity mainActivity;
    String origin;

    public ProjectCreatedFromDocumentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_project_created_confirmation, container, false);

        mainActivity = (MainActivity)getActivity();
        MainActivity.titleName.setText("Add Project");

        String name = getArguments().getString("name");
        origin = getArguments().getString("origin");

        TextView textView = (TextView)rootView.findViewById(R.id.createdConfirmTextView);
        Button button = (Button)rootView.findViewById(R.id.createdConfirmButton);

        textView.setText("\"" + name + "\" has been created.");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.backToCurrentDocument();
            }
        });

        return rootView;
    }

}
