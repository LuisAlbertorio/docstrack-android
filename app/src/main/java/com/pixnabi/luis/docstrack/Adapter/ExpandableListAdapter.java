package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Luis on 1/18/2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    Context context;
    JSONArray data;

    public ExpandableListAdapter(Context context){
        this.context = context;
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return (data == null) ? 0 : data.length();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return data.getJSONObject(groupPosition).getJSONArray("approvers").length();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        try {
            return "Workflow " + data.getJSONObject(groupPosition).getString("roundNumber");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        try {
            return data.getJSONObject(groupPosition).getJSONArray("approvers").getString(childPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object getStatusColor(int groupPosition, int childPosition){
        try {
            return data.getJSONObject(groupPosition).getJSONArray("approverStatuses").getString(childPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

//        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_group_header, null);
            TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
            String listTitle = (String)getGroup(groupPosition);
            listTitleTextView.setText(listTitle);

            return convertView;
//        }else{
//
//            return convertView;
//        }
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

//        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_child_items, null);

            String childText = (String)getChild(groupPosition, childPosition);
            String statusText = (String)getStatusColor(groupPosition,childPosition);

            TextView textView = (TextView)convertView.findViewById(R.id.expandedListItem);

            ImageView status = (ImageView)convertView.findViewById(R.id.expandedListImageStatus);

//            try {

                if(statusText.equals("Off") || statusText.equals("Stopped")){
                    status.setImageResource(R.drawable.gray_blank_circle);
                }else if(statusText.equals("In Review")){
                    status.setImageResource(R.drawable.yellow_blank_circle);
                }else if(statusText.equals("Approved")){
                    status.setImageResource(R.drawable.green_blank_circle);
                }else if(statusText.equals("Rejected")){
                    status.setImageResource(R.drawable.red_blank_circle);
                }

//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            textView.setText(childText);


            return convertView;
//        }else{
//
//            return convertView;
//        }

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
