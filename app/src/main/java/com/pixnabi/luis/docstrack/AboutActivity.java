package com.pixnabi.luis.docstrack;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {


    TextView webAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        webAddress = (TextView)findViewById(R.id.aboutWebsiteTextView);

        SpannableString content = new SpannableString("http://itcspr.com/");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

        webAddress.setText(content);

        webAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(webAddress.getText().toString()));
                startActivity(i);
            }
        });
    }
}
