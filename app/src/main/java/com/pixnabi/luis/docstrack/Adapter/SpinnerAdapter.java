package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Luis on 12/12/2016.
 */

public class SpinnerAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray jsonArray;
    String origin;

    public SpinnerAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray jsonArray, String origin){
        this.jsonArray = jsonArray;
        this.origin = origin;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (jsonArray == null) ? 0 : jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_dropdowns, parent, false);

        TextView textView = (TextView)rowView.findViewById(R.id.dropdownTextView);

        if(origin.equals("addUserDept")){
            textView.setGravity(Gravity.END);
            try {
                textView.setText(jsonArray.getJSONObject(position).get("name").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("projects")){
            try {
                textView.setText(jsonArray.getJSONObject(position).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(origin.equals("departments")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("name").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(origin.equals("notifications")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("time").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(origin.equals("docTypes")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("type").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(origin.equals("approvers")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("aFullName").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(origin.equals("editDocumentProjects")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("name").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("editDocDocSpinner")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("name").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("docName")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("name").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("docNumber")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("number").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("proName")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("name").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("proNumber")){
            try {
                textView.setText(jsonArray.getJSONObject(position).get("number").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(origin.equals("userData")){
            try {
                textView.setText(jsonArray.getJSONObject(position).getString("uFullName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return rowView;
    }
}
