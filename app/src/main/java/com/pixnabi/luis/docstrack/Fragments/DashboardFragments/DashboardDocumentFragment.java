package com.pixnabi.luis.docstrack.Fragments.DashboardFragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Adapter.DashboardDocumentAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardDocumentFragment extends Fragment {

    MainActivity mainActivity;

    public DashboardDocumentFragment() {
        // Required empty public constructor
    }

//    @Override
//    public void onAttach(Context context) {
//        mainActivity = (MainActivity)context;
//        super.onAttach(context);
//    }

    SharedPreferences sharedPreferences;
    ListView listView;
    DashboardDocumentAdapter adapter;
    TextView projectName;
    LinearLayout noDocs;
    Button createDoc;
    ImageView status;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_dashboard_document, container, false);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        listView = (ListView)rootView.findViewById(R.id.dashboardDocumentListView);
        projectName = (TextView)rootView.findViewById(R.id.dashboardDocumentProjectNameTextView);
        noDocs = (LinearLayout)rootView.findViewById(R.id.dashboardDocumentNoDocsLinearLayout);
        createDoc = (Button)rootView.findViewById(R.id.dashboardDocumentCreateDocumentButton);
        status = (ImageView)rootView.findViewById(R.id.dashboardDocumentStatus);

        mainActivity = (MainActivity)getActivity();

        adapter = new DashboardDocumentAdapter(getActivity());
        String projectId = getArguments().getString("projectId");
        listView.setAdapter(adapter);

        MainActivity.progressBar.setVisibility(View.VISIBLE);
        MainActivity.titleName.setText("Dashboard");

        projectName.setText(getArguments().getString("projectName"));

        if(getArguments().getString("status").equals("Approved")){
            status.setImageResource(R.drawable.green_blank_circle);
        }else if(getArguments().getString("status").equals("In Review")){
            status.setImageResource(R.drawable.yellow_blank_circle);
        }else if(getArguments().getString("status").equals("Rejected")){
            status.setImageResource(R.drawable.red_blank_circle);
        }else if(getArguments().getString("status").equals("Awaiting Start")){
            status.setImageResource(R.drawable.gray_blank_circle);
        }

        if(sharedPreferences.getString("role", null).equals("Viewer") || sharedPreferences.getString("role", null).equals("Approver") ){
            createDoc.setVisibility(View.GONE);
        }


        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    adapter.setData(jsonArray);
                    MainActivity.progressBar.setVisibility(View.INVISIBLE);
                    Log.wtf("ADAPTCOUNT", String.valueOf(adapter.getCount()));
                    if (adapter.getCount() == 0){
                        noDocs.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("dashboardDocumentShow", projectId, sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null), sharedPreferences.getString("fullName",null), sharedPreferences.getString("companyId", null));


        createDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.CreateDocument("dashboard");
                Log.wtf("Error", "click");
            }
        });


        return rootView;
    }

}
