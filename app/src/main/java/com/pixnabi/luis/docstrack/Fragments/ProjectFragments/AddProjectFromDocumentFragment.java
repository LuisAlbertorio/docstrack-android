package com.pixnabi.luis.docstrack.Fragments.ProjectFragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Adapter.OwnersAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProjectFromDocumentFragment extends Fragment {

    static TextView dateTextView;
    EditText projectNumber, projectName;
    Button createProject;
    Spinner ownersSpinner;
    OwnersAdapter adapter;

    SharedPreferences sharedPreferences;

    MainActivity mainActivity;

    ProgressDialog progressDialog;
    JSONArray data;

    LinearLayout ownerLL;

//    String origin;
    public static boolean resume = false;

    @Override
    public void onResume() {
        super.onResume();
        if(resume){
            resume = false;
            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {

                    try {
                        data = new JSONArray(result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    adapter.setData(data);
                    progressDialog.dismiss();
                }
            }).execute("dropdownOwners", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null));
        }
    }

    public AddProjectFromDocumentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_project, container, false);
        dateTextView = (TextView)rootView.findViewById(R.id.addProjectDateTextView);
        projectNumber = (EditText)rootView.findViewById(R.id.addProjectProjectNumber);
        projectName = (EditText)rootView.findViewById(R.id.addProjectProjectName);
        createProject = (Button)rootView.findViewById(R.id.addProjectCreateButton);
        ownersSpinner = (Spinner)rootView.findViewById(R.id.addProjectOwnerSpinner);
        ownerLL = (LinearLayout)rootView.findViewById(R.id.addprojectOwnerLinearLayout);

        MainActivity.titleName.setText("Add Project");

        mainActivity = (MainActivity)getActivity();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Getting Info...");
        progressDialog.show();

//        origin = getArguments().getString("origin");

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        if(sharedPreferences.getString("role", null).equals("Owner")){
            ownerLL.setVisibility(View.GONE);
        }

        adapter = new OwnersAdapter(getActivity());
        ownersSpinner.setAdapter(adapter);

        dateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new AddProjectFromDocumentFragment.SelectDateFragment();
                newFragment.show(mainActivity.getFragmentManager(), "DatePicker");

            }
        });

        createProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateClicked();
            }
        });

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {

                try {
                    data = new JSONArray(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.setData(data);
                progressDialog.dismiss();
            }
        }).execute("dropdownOwners", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null));

        return rootView;
    }

    public void onCreateClicked(){

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Creating Project...");
        progressDialog.show();

        try {

            if(sharedPreferences.getString("role", null).equals("Owner")){
                Log.wtf("TEST", "OWNER ROLE");
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.has("msg")){
                                System.out.println("success");
                                mainActivity.confirmCreatedProjectFromDocument(projectName.getText().toString());
                                progressDialog.dismiss();
                            }else{
                                System.out.println("failure");
                                progressDialog.dismiss();
                                ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("projectCreate", projectName.getText().toString(), projectNumber.getText().toString(), dateTextView.getText().toString(), sharedPreferences.getString("fullName",null), sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("role", null), sharedPreferences.getString("fullName", null));
            }else{
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.has("msg")){
                                System.out.println("success");
                                mainActivity.confirmCreatedProjectFromDocument(projectName.getText().toString());
                                progressDialog.dismiss();
                            }else{
                                System.out.println("failure");
                                progressDialog.dismiss();
                                ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("projectCreate", projectName.getText().toString(), projectNumber.getText().toString(), dateTextView.getText().toString(), data.getJSONObject(ownersSpinner.getSelectedItemPosition()).getString("oFullName"), sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("role", null), sharedPreferences.getString("fullName", null));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("ValidFragment")
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            dateTextView.setText(month + "/" + day + "/" + year);
        }
    }

}
