package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

/**
 * Created by Luis on 1/4/2017.
 */

public class AddUserRolesAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;

    String[] data = {"", "Owner", "Viewer", "Approver"};

    public AddUserRolesAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_dropdown_roles, parent, false);

        TextView name = (TextView)rowView.findViewById(R.id.dropdownRolesTextView);

        name.setText(data[position]);

        return rowView;
    }
}
