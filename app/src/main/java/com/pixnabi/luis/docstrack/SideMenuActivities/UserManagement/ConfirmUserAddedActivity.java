package com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

public class ConfirmUserAddedActivity extends AppCompatActivity {

    TextView confirm;
    Button userAdded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_confirm_user_added);

        confirm = (TextView)findViewById(R.id.confirmUserAddedTextView);
        userAdded = (Button)findViewById(R.id.createdConfirmUserButton);

        confirm.setText(getIntent().getExtras().getString("msg"));

        userAdded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AddUserActivity.activityObj.finish();
                finish();
            }
        });

    }
}
