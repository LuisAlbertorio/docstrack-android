package com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList;

import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

public class SystemCustomListActivity extends AppCompatActivity {

    ImageButton departments, documentTypes, notifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }


        setContentView(R.layout.activity_system_custom_list);

        departments = (ImageButton)findViewById(R.id.sysCustomDepartments);
        documentTypes = (ImageButton)findViewById(R.id.sysCustomDocTypes);
        notifications = (ImageButton)findViewById(R.id.sysCustomNotifications);

        documentTypes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDocsClicked();
            }
        });

        departments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDepartmentsClicked();
            }
        });

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNotificationsClicked();
            }
        });
    }

    public void onDepartmentsClicked(){
        if(!MainActivity.sharedPreferences.getString("role", null).equals("Owner")){
            Intent intent = new Intent(this, DepartmentsActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, "You don't have permission to access this area.", Toast.LENGTH_LONG).show();
        }
    }

    public void onDocsClicked(){
        Intent intent = new Intent(this, DocumentTypesActivity.class);
        startActivity(intent);
    }

    public void onNotificationsClicked(){
        Intent intent = new Intent(this, NotificationsActivity.class);
        startActivity(intent);
    }
}
