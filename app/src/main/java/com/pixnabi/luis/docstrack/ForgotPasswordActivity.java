package com.pixnabi.luis.docstrack;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText email, question1Answer, question2Answer;
    TextView question1, question2;
    Button checkEmail, submitQuestions;

    LinearLayout showQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        email = (EditText)findViewById(R.id.forgotPassEmailEditText);
        question1Answer = (EditText)findViewById(R.id.forgotPassQuestion1EditText);
        question2Answer = (EditText)findViewById(R.id.forgotPassQuestion2EditText);
        question1 = (TextView)findViewById(R.id.forgotPassQuestion1TextView);
        question2 = (TextView)findViewById(R.id.forgotPassQuestion2TextView);
        checkEmail = (Button)findViewById(R.id.forgotPassCheckEmailButton);
        submitQuestions = (Button)findViewById(R.id.forgotPassSubmitButton);
        showQuestions = (LinearLayout)findViewById(R.id.forgotPassSecurityQuestionsLayout);

        checkEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCheckPressed();
            }
        });

        submitQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmitPressed();
            }
        });
    }

    public void onCheckPressed(){

        String emailStr = email.getText().toString();

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Verifying Email..");
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String[] resultArray = result.split("(?<=\\})");

                    if(jsonObject.has("error")){
                        ErrorDialog.error(ForgotPasswordActivity.this, jsonObject.getString("error"));
                    }else{
                        JSONObject jsonquestion0 = new JSONObject(resultArray[0]);
                        JSONObject jsonquestion1 = new JSONObject(resultArray[1]);

                        question1.setText(jsonquestion0.getString("sq1"));
                        question2.setText(jsonquestion1.getString("sq1"));

                        showQuestions.setVisibility(View.VISIBLE);
                        checkEmail.setVisibility(View.GONE);

                        email.setEnabled(false);
                    }

                    System.out.println(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).execute("forgotPassVerifyEmail", emailStr);


    }

    public void onSubmitPressed(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Checking answers..");
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                System.out.println(result);

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if(!jsonObject.isNull("result")){
                        progressDialog.dismiss();
                        Intent intent = new Intent(ForgotPasswordActivity.this, ForgotPassContinuar.class);
                        startActivity(intent);
                        LogInActivity.activityObj.finish();
                        finish();
                    }else{
                        progressDialog.dismiss();
                        ErrorDialog.error(ForgotPasswordActivity.this, jsonObject.getString("error"));
//                        Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).execute("forgotPassVerifyQuestions", question1Answer.getText().toString(), question2Answer.getText().toString(), email.getText().toString());

    }
}
