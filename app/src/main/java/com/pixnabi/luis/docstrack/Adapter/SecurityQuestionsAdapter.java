package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

/**
 * Created by Luis on 11/9/2016.
 */

public class SecurityQuestionsAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    public String[] questions = {"Who was your childhood hero?",
            "What was the name of the company where you had your first job?",
            "What time of the day were you born?(hh:mm)",
            "In what city or town did your parents meet?",
            "What is the name of your favorite childhood friend?",
            "In what town or city was your first full-time job?",
            "What was the make and model of your first car?",
            "What was your favorite sport in high school?",
            "What school did you attend for sixth grade?"};

    public SecurityQuestionsAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return questions.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_security_questions,parent,false);

        TextView textView = (TextView)rowView.findViewById(R.id.securityQuestionsTextView);
        textView.setText(questions[position]);

        return rowView;
    }
}
