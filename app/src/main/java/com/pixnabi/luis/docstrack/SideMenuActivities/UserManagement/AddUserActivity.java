package com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.AddUserDepartmentDropdown;
import com.pixnabi.luis.docstrack.Adapter.AddUserRolesAdapter;
import com.pixnabi.luis.docstrack.Adapter.DocumentDepartmentDropdown;
import com.pixnabi.luis.docstrack.Adapter.SpinnerAdapter;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddUserActivity extends AppCompatActivity {

    EditText firstName, lastName, phone, email;
    TextView infoRole;
    Spinner role, department;
    Button addUser;
    AddUserRolesAdapter adapter;

    String[] roles = {"", "Owner", "Viewer", "Approver"};
    String[] infoArray = {"This message will display the information, security and accesibility of the role that is selected.", "Owners get full access to the application, except for editing company information and editing or deleting existing users. Dashboard visibility and reporting is limited to Projects and Documents they created.", "Viewers get unique access to all Projects and Documents of the company and are able to generate reports on them, but have no participation in Workflows.", "Approvers are participants of Workflows as assigned by the Owners. Dashboard visibility and reporting is limited to their assigned work."};

    SharedPreferences sharedPreferences;

//    SpinnerAdapter spinnerAdapter;

    JSONArray deptData;

//    DocumentDepartmentDropdown documentDepartmentDropdown;
    AddUserDepartmentDropdown addUserDepartmentDropdown;

    public static Activity activityObj;
    public static boolean resumeDept=false;

    @Override
    protected void onResume() {
        super.onResume();
        if(resumeDept){
            resumeDept = false;
            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        deptData = jsonArray;
                        addUserDepartmentDropdown.setData(jsonArray, "addUserDept");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_add_user);

        firstName = (EditText) findViewById(R.id.addUserFirstName);
        lastName = (EditText) findViewById(R.id.addUserLastName);
        phone = (EditText) findViewById(R.id.addUserPhone);
        email = (EditText) findViewById(R.id.addUserEmail);
        infoRole = (TextView) findViewById(R.id.addUserInfoRoleText);
        role = (Spinner) findViewById(R.id.addUserRoleSpinner);
        addUser = (Button) findViewById(R.id.addUserAddButton);
        department = (Spinner) findViewById(R.id.addUserDepartmentSpinner);
        adapter = new AddUserRolesAdapter(this);
        addUserDepartmentDropdown = new AddUserDepartmentDropdown(this);
//        spinnerAdapter = new SpinnerAdapter(this);
        role.setAdapter(adapter);
        department.setAdapter(addUserDepartmentDropdown);

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddUserClicked();
            }
        });

        role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                infoRole.setText(infoArray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    deptData = jsonArray;
                    addUserDepartmentDropdown.setData(jsonArray, "addUserDept");
//                    spinnerAdapter.setData(jsonArray, "addUserDept");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));


        activityObj = this;
    }

    public void onAddUserClicked() {

        String first = firstName.getText().toString();
        String last = lastName.getText().toString();
        String phoneNumber = phone.getText().toString();
        String fullEmail = email.getText().toString();
        String adminName = sharedPreferences.getString("fullName", null);
        String adminRole = sharedPreferences.getString("role", null);
        String adminCompanyId = sharedPreferences.getString("companyId", null);

        if (first.equals("") || last.equals("") || phoneNumber.equals("") || fullEmail.equals("") || roles[role.getSelectedItemPosition()].equals("")) {
            ErrorDialog.error(AddUserActivity.this, "Fields Can't be empty");
        } else {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Adding User...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {

                            JSONObject jsonObject = new JSONObject(result);
                            progressDialog.dismiss();

                            if (jsonObject.has("error")) {
                                ErrorDialog.error(AddUserActivity.this, jsonObject.getString("error"));

                            } else {
                                Intent intent = new Intent(AddUserActivity.this, ConfirmUserAddedActivity.class);
                                intent.putExtra("msg", jsonObject.getString("msg"));
                                startActivity(intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("userCreate", first, last, phoneNumber, fullEmail, roles[role.getSelectedItemPosition()], adminName, adminRole, adminCompanyId, sharedPreferences.getString("userId", null), deptData.getJSONObject(department.getSelectedItemPosition()-1).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
