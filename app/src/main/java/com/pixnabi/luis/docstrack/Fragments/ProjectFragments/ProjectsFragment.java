package com.pixnabi.luis.docstrack.Fragments.ProjectFragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.TestDragDropActivity;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import java.util.Arrays;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsFragment extends Fragment {


    public ProjectsFragment() {
        // Required empty public constructor
    }

    ImageButton createProject, editProject, createDocuments, editDocuments;
    SharedPreferences sharedPreferences;
    MainActivity mainActivity;


    @Override
    public void onAttach(Context context) {
//        mainActivity = (MainActivity)context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_projects, container, false);

        createProject = (ImageButton) rootview.findViewById(R.id.projectsCreateButton);
        editProject = (ImageButton) rootview.findViewById(R.id.projectsEditButton);
        createDocuments = (ImageButton) rootview.findViewById(R.id.projectsAddDocuments);
        editDocuments = (ImageButton) rootview.findViewById(R.id.projectEditDocument);

        MainActivity.titleName.setText("Documents");

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        mainActivity = (MainActivity)getActivity();

        createProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreatePressed();
            }
        });

        editProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditPressed();
            }
        });

        createDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDocumentCreatePressed();
            }
        });

        editDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDocumentEditPressed();
            }
        });

        return rootview;
    }

    public void onCreatePressed(){
        mainActivity.NewProject("normal");
    }

    public void onEditPressed(){
        mainActivity.EditProject();
    }

    public void onDocumentCreatePressed(){
        mainActivity.CreateDocument("normal");
    }

    public void onDocumentEditPressed(){
        mainActivity.EditDocument();
    }

}
