package com.pixnabi.luis.docstrack;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class LogInActivity extends AppCompatActivity {

    TextView forgotPass;
    Button logIn, signUp;
    EditText email, password;

    public static Activity activityObj;

    String oneSignalUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_log_in);

        signUp = (Button)findViewById(R.id.logInSignUpTextView);
        forgotPass = (TextView)findViewById(R.id.logInForgotPassTextView);
        logIn = (Button)findViewById(R.id.logInLogInButton);
        email = (EditText)findViewById(R.id.logInEmailEditText);
        password = (EditText)findViewById(R.id.logInPasswordEditText);

        activityObj = this;

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginPressed();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSignUpPressed();
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onForgotPassPressed();
            }
        });

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                oneSignalUserId = userId;
                Log.d("OneUserId",oneSignalUserId);
            }
        });

    }

    public void onSignUpPressed(){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    public void onLoginPressed(){
        String emailString = email.getText().toString();
        final String passString = password.getText().toString();
        String type = "login";

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging in..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                System.out.println(result);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.has("error")){
                        ErrorDialog.error(LogInActivity.this, jsonObject.getString("error"));
                    }else{
                        try {
                            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email", jsonObject.getString("email"));
                            editor.putString("tokenId", jsonObject.getString("tokenId"));
                            editor.putString("userId", jsonObject.get("userId").toString());
                            editor.putString("companyId", jsonObject.get("companyId").toString());
                            editor.putString("isActive", jsonObject.get("isActive").toString());
                            editor.putString("role", jsonObject.getString("role"));
                            editor.putString("fullName", jsonObject.getString("fullName"));
                            editor.putString("photo", jsonObject.getString("profilePicture"));
                            editor.putString("department", jsonObject.getString("department"));
                            editor.putString("password", passString);
                            editor.apply();

                            System.out.println(String.valueOf(jsonObject));

                            if(Integer.parseInt(jsonObject.get("isActive").toString()) == 0){
                                Intent intent = new Intent(LogInActivity.this, UserProfileFirstTimeActivity.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            System.out.println("START");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();

            }
        }).execute(type, emailString, passString, oneSignalUserId);

    }

    public void onForgotPassPressed(){
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }
}
