package com.pixnabi.luis.docstrack.Fragments.ReportsFragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.Adapter.AuditTrailUsersAdapter;
import com.pixnabi.luis.docstrack.Adapter.SpinnerAdapter;
import com.pixnabi.luis.docstrack.Adapter.WorkflowDistUsersAdapter;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.AddProjectFragment;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportsFragment extends Fragment {


    public ReportsFragment() {
        // Required empty public constructor
    }

    Spinner spinner;
    Spinner doctypeSpinner, docNumberSpinner, projectNumberSpinner, userNameSpinner, workflowDistRoleSpinner, workflowDistUsernameSpinner;
    SpinnerAdapter adapter1, adapter3, adapter5;
    String[] reportTypes = {"Summary by Document Type", "Document Time Distribution", "Project Time Distribution", "Audit Trail", "Workflow Distribution by Role"};
    String[] roles = {"Administrator", "Owner", "Approver"};
    LinearLayout layout1, layout2, layout3, layout4, layout5;
    SharedPreferences sharedPreferences;
    JSONArray doctypesData, docNumberData, docNameData, proNameData, proNumberData, userData, workflowUserData;
    ProgressDialog progressDialog;
    Button generateReport;
    static TextView auditTrailFromDate, auditTrailToDate;
    static TextView docTypeFromDate, docTypeToDate;
    MainActivity mainActivity;
    TextView docNameTextView, projectNameTextView;
    JSONArray projectData;
    JSONArray docData;
    AuditTrailUsersAdapter auditTrailUsersAdapter;
    WorkflowDistUsersAdapter workflowDistUsersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_reports, container, false);
        spinner = (Spinner)rootview.findViewById(R.id.reportsTypesSpinner);
        layout1 = (LinearLayout)rootview.findViewById(R.id.reportsLayout1);
        layout2 = (LinearLayout)rootview.findViewById(R.id.reportsLayout2);
        layout3 = (LinearLayout)rootview.findViewById(R.id.reportsLayout3);
        layout4 = (LinearLayout)rootview.findViewById(R.id.reportsLayout4);
        layout5 = (LinearLayout)rootview.findViewById(R.id.reportsLayout5);
        doctypeSpinner = (Spinner)rootview.findViewById(R.id.reportsDoctypeSpinner);
//        docNameSpinner = (Spinner)rootview.findViewById(R.id.reportsDocNameSpinner);
        docNumberSpinner = (Spinner)rootview.findViewById(R.id.reportsDocNumberSpinner);
//        projectNameSpinner = (Spinner)rootview.findViewById(R.id.reportsProjectNameSpinner);
        projectNumberSpinner = (Spinner)rootview.findViewById(R.id.reportsProjectNumberSpinner);
        userNameSpinner = (Spinner)rootview.findViewById(R.id.reportsUsernameAuditTrailSpinner);
        workflowDistRoleSpinner = (Spinner)rootview.findViewById(R.id.reportsWorkflowDistributionRoleSpinner);
        workflowDistUsernameSpinner = (Spinner)rootview.findViewById(R.id.reportsWorkflowDistributionUsernameWorkflowSpinner);
        generateReport = (Button)rootview.findViewById(R.id.reportsGenerateReportButton);
        auditTrailFromDate = (TextView)rootview.findViewById(R.id.reportsFromDateButton);
        auditTrailToDate = (TextView)rootview.findViewById(R.id.reportsToDateButton);
        docTypeFromDate = (TextView)rootview.findViewById(R.id.reportsFromDateDocType);
        docTypeToDate = (TextView)rootview.findViewById(R.id.reportsToDateDocType);
        docNameTextView = (TextView)rootview.findViewById(R.id.reportsDocNameTextView);
        projectNameTextView = (TextView)rootview.findViewById(R.id.reportsProjectNameTextView);
        progressDialog = new ProgressDialog(getActivity());
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, reportTypes);
        ArrayAdapter<String> stringArrayAdapterRoles = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, roles);
        spinner.setAdapter(stringArrayAdapter);
        workflowDistRoleSpinner.setAdapter(stringArrayAdapterRoles);
        adapter1 = new SpinnerAdapter(getActivity());
//        adapter2 = new SpinnerAdapter(getActivity());
        adapter3 = new SpinnerAdapter(getActivity());
//        adapter4 = new SpinnerAdapter(getActivity());
        adapter5 = new SpinnerAdapter(getActivity());
        auditTrailUsersAdapter = new AuditTrailUsersAdapter(getActivity());
//        adapter6 = new SpinnerAdapter(getActivity());
        workflowDistUsersAdapter = new WorkflowDistUsersAdapter(getActivity());
//        adapter7 = new SpinnerAdapter(getActivity());
        doctypeSpinner.setAdapter(adapter1);
//        docNameSpinner.setAdapter(adapter2);
        docNumberSpinner.setAdapter(adapter3);
//        projectNameSpinner.setAdapter(adapter4);
        projectNumberSpinner.setAdapter(adapter5);
        userNameSpinner.setAdapter(auditTrailUsersAdapter);
//        userNameSpinner.setAdapter(adapter6);
        workflowDistUsernameSpinner.setAdapter(workflowDistUsersAdapter);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        MainActivity.titleName.setText("Reports");

        mainActivity = (MainActivity)getActivity();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getLayoutAcordingtoSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        generateReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GenerateReport();
            }
        });

        auditTrailToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragmentForTo();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        auditTrailFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        docTypeFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectFromDateDocTypeFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        docTypeToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectToDateDocTypeFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        projectNumberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    projectNameTextView.setText(projectData.getJSONObject(position).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        docNumberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    docNameTextView.setText(docData.getJSONObject(position).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return rootview;
    }

    public void getLayoutAcordingtoSpinner(){

        progressDialog.setMessage("Getting Info...");
        progressDialog.show();

        if(spinner.getSelectedItemPosition() == 0){
            layout1.setVisibility(View.VISIBLE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.GONE);
            layout4.setVisibility(View.GONE);
            layout5.setVisibility(View.GONE);

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        doctypesData = jsonArray;
                        adapter1.setData(jsonArray, "docTypes");
                        progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).execute("DropdownDocumentTypes", sharedPreferences.getString("companyId", null));


        }else if(spinner.getSelectedItemPosition() == 1){
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.VISIBLE);
            layout3.setVisibility(View.GONE);
            layout4.setVisibility(View.GONE);
            layout5.setVisibility(View.GONE);

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    try {
//                        docNameData = new JSONArray();
                        docNumberData = new JSONArray();
                        JSONArray jsonArray = new JSONArray(result);
                        docData = jsonArray;
                        for(int i=0; i<jsonArray.length(); i++){
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("name", jsonArray.getJSONObject(i).getString("name"));
//                            docNameData.put(jsonObject);

                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("number", jsonArray.getJSONObject(i).getString("number"));
                            docNumberData.put(jsonObject1);
                        }

//                        adapter2.setData(docNameData, "docName");
                        adapter3.setData(docNumberData, "docNumber");

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).execute("DocumentTimeDistribution", sharedPreferences.getString("role", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null));

        }else if(spinner.getSelectedItemPosition() == 2){
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.VISIBLE);
            layout4.setVisibility(View.GONE);
            layout5.setVisibility(View.GONE);

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    proNameData = new JSONArray();
                    proNumberData = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        projectData = jsonArray;
                        for(int i=0; i<jsonArray.length(); i++){
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("name", jsonArray.getJSONObject(i).getString("name"));
//                            proNameData.put(jsonObject);

                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("number", jsonArray.getJSONObject(i).getString("number"));
                            proNumberData.put(jsonObject1);
                        }

//                        adapter4.setData(proNameData, "proName");
                        adapter5.setData(proNumberData, "proNumber");

                        progressDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).execute("ProjectTimeDistribution", sharedPreferences.getString("role", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("userId", null));

        }else if(spinner.getSelectedItemPosition() == 3){
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.GONE);
            layout4.setVisibility(View.VISIBLE);
            layout5.setVisibility(View.GONE);

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    userData = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        userData = jsonArray;
                        auditTrailUsersAdapter.setData(userData);
//                        adapter6.setData(userData, "userData");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();

                }
            }).execute("AuditTrailUsers", sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null));


        }else if(spinner.getSelectedItemPosition() == 4){
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.GONE);
            layout4.setVisibility(View.GONE);
            layout5.setVisibility(View.VISIBLE);

            workflowDistRoleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    roleChanging(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    progressDialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        workflowUserData = jsonArray;
                        workflowDistUsersAdapter.setData(jsonArray);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).execute("WorkflowDistributionReportUsers", "Administrator", sharedPreferences.getString("role", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("userId", null));
        }
    }

    public void roleChanging(int position){
        progressDialog.show();
        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    progressDialog.dismiss();
                    JSONArray jsonArray = new JSONArray(result);
                    workflowUserData = jsonArray;
                    workflowDistUsersAdapter.setData(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("WorkflowDistributionReportUsers", roles[position], sharedPreferences.getString("role", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("userId", null));
    }

    public void GenerateReport(){
        progressDialog.setMessage("Generating Report...");
        progressDialog.show();

        if(layout1.getVisibility() == View.VISIBLE){

            if(docTypeToDate.getText().toString().equals("Enter Date") || docTypeFromDate.getText().toString().equals("Enter Date")){
                Toast.makeText(getActivity(), "Please enter both dates", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }else{
                Log.wtf("toDate",auditTrailToDate.getText().toString().replace("/", "-"));
                String[] todate = docTypeToDate.getText().toString().split("/");
                String[] fromdate = docTypeFromDate.getText().toString().split("/");
                String todateChange = todate[1] + "-" + todate[0] + "-" + todate[2];
                String fromdateChange = fromdate[1] + "-" + fromdate[0] + "-" + fromdate[2];

                try {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                progressDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(result);
                                mainActivity.goToReportsWebView(jsonObject.getString("link"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("GenerateReport","Summary by Document Type", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "", "", doctypesData.getJSONObject(doctypeSpinner.getSelectedItemPosition()).getString("type"), fromdateChange, todateChange,"","","","","","");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }else if(layout2.getVisibility() == View.VISIBLE){

            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(result);
                            mainActivity.goToReportsWebView(jsonObject.getString("link"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("GenerateReport","Document Time Distribution", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "",docNumberData.getJSONObject(docNumberSpinner.getSelectedItemPosition()).getString("number"), "","","","","","","","","");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if(layout3.getVisibility() == View.VISIBLE){

            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(result);
                            mainActivity.goToReportsWebView(jsonObject.getString("link"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("GenerateReport","Project Time Distribution", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), proNumberData.getJSONObject(projectNumberSpinner.getSelectedItemPosition()).getString("number"), "", "","","","","","","","","");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if(layout4.getVisibility() == View.VISIBLE){

            if(auditTrailFromDate.getText().toString().equals("Enter Date") || auditTrailToDate.getText().toString().equals("Enter Date")){
                Toast.makeText(getActivity(), "Please enter both dates", Toast.LENGTH_LONG).show();
            }else{
                if(userNameSpinner.getSelectedItemPosition() == 0){


                    Log.wtf("AlltoDate",auditTrailToDate.getText().toString().replace("/", "-"));

                    Log.wtf("toDate",auditTrailToDate.getText().toString().replace("/", "-"));
                    String[] todate = auditTrailToDate.getText().toString().split("/");
                    String[] fromdate = auditTrailFromDate.getText().toString().split("/");
                    String todateChange = todate[1] + "-" + todate[0] + "-" + todate[2];
                    String fromdateChange = fromdate[1] + "-" + fromdate[0] + "-" + fromdate[2];

                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                progressDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(result);
                                mainActivity.goToReportsWebView(jsonObject.getString("link"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("GenerateReport","Audit Trail", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "", "", "","","", "All", fromdateChange, todateChange,"","","");
                }else{

                    Log.wtf("toDate",auditTrailToDate.getText().toString().replace("/", "-"));
                    String[] todate = auditTrailToDate.getText().toString().split("/");
                    String[] fromdate = auditTrailFromDate.getText().toString().split("/");
                    String todateChange = todate[1] + "-" + todate[0] + "-" + todate[2];
                    String fromdateChange = fromdate[1] + "-" + fromdate[0] + "-" + fromdate[2];

                    try {
                        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                            @Override
                            public void processFinish(String result) {
                                try {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject = new JSONObject(result);
                                    mainActivity.goToReportsWebView(jsonObject.getString("link"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).execute("GenerateReport","Audit Trail", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "", "", "","","", userData.getJSONObject(userNameSpinner.getSelectedItemPosition()-1).getString("id"), fromdateChange, todateChange,"","","");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }



        }else if(layout5.getVisibility() == View.VISIBLE){
            if(workflowDistUsernameSpinner.getSelectedItemPosition() == 0){
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(result);
                            mainActivity.goToReportsWebView(jsonObject.getString("link"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("GenerateReport","Workflow Distribution by Role", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "", "", "","","","","","", roles[workflowDistRoleSpinner.getSelectedItemPosition()], "All", "All");
            }else{
                try {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                progressDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(result);
                                mainActivity.goToReportsWebView(jsonObject.getString("link"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("GenerateReport","Workflow Distribution by Role", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "", "", "","","","","","", roles[workflowDistRoleSpinner.getSelectedItemPosition()], workflowUserData.getJSONObject(workflowDistUsernameSpinner.getSelectedItemPosition()-1).getString("uFullName"), workflowUserData.getJSONObject(workflowDistUsernameSpinner.getSelectedItemPosition()-1).getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            auditTrailFromDate.setText(month + "/" + day + "/" + year);
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectDateFragmentForTo extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            auditTrailToDate.setText(month + "/" + day + "/" + year);
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectFromDateDocTypeFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            docTypeFromDate.setText(month + "/" + day + "/" + year);
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectToDateDocTypeFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            docTypeToDate.setText(month + "/" + day + "/" + year);
        }
    }

}
