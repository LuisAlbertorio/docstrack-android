package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.AddProjectFragment;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement.AddUserActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

/**
 * Created by Luis on 1/16/2017.
 */

public class OwnersAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray data;

    public OwnersAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (data == null) ? 1 : data.length() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        if(position == getCount()-1){
            View rowView = inflater.inflate(R.layout.row_add_owner, parent, false);

            Button addOwner = (Button)rowView.findViewById(R.id.rowAddOwnerAddButton);

            addOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddProjectFragment.resume = true;
                    Intent intent = new Intent(context, AddUserActivity.class);
                    context.startActivity(intent);
                }
            });

            return rowView;

        }else{
            View rowView = inflater.inflate(R.layout.row_dropdowns, parent, false);

            TextView name = (TextView)rowView.findViewById(R.id.dropdownTextView);

            try {
                name.setText(data.getJSONObject(position).getString("oFullName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return rowView;
        }
    }
}
