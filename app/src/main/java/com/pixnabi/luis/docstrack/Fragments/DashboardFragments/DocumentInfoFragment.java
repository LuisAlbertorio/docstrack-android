package com.pixnabi.luis.docstrack.Fragments.DashboardFragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Adapter.DashboardInfoAdapter;
import com.pixnabi.luis.docstrack.Adapter.ExpandableListAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class DocumentInfoFragment extends Fragment {


    public DocumentInfoFragment() {
        // Required empty public constructor
    }

    String docId;
    SharedPreferences sharedPreferences;
    TextView docName, docOwner;
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_document_info, container, false);

        docName = (TextView)rootView.findViewById(R.id.docInfoDocNameTextView);
        docOwner = (TextView)rootView.findViewById(R.id.docInfoDocOwnerTextView);
        expandableListView = (ExpandableListView)rootView.findViewById(R.id.docInfoExpandableListView);
        expandableListAdapter = new ExpandableListAdapter(getActivity());
        expandableListView.setAdapter(expandableListAdapter);

        MainActivity.progressBar.setVisibility(View.VISIBLE);
        MainActivity.titleName.setText("Dashboard");

        docName.setText(getArguments().getString("docName"));
        docOwner.setText(getArguments().getString("docOwner"));

        docId = getArguments().getString("docId");
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    expandableListAdapter.setData(jsonObject.getJSONArray("rounds"));
                    expandableListView.expandGroup(jsonObject.getJSONArray("rounds").length()-1);
                    MainActivity.progressBar.setVisibility(View.INVISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("dashboardDocumentInfoShow", sharedPreferences.getString("companyId", null), docId);

        return rootView;
    }

}
