package com.pixnabi.luis.docstrack.Utility;

import com.onesignal.OneSignal;

/**
 * Created by Luis on 1/23/2017.
 */

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this).init();

        OneSignal.setInFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);

    }
}
