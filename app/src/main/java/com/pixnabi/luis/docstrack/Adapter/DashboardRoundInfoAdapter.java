package com.pixnabi.luis.docstrack.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Luis on 1/17/2017.
 */

public class DashboardRoundInfoAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    JSONArray approvers;
    JSONArray statuses;

    public DashboardRoundInfoAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(JSONArray approvers, JSONArray statuses){
        this.approvers = approvers;
        this.statuses = statuses;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (approvers == null) ? 0 : approvers.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.row_round_info, parent, false);

        TextView approverName = (TextView)rowView.findViewById(R.id.rowRoundInfoApproverTextView);

        try {
            approverName.setText(approvers.getString(position) + ", " + statuses.getString(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        for(int i =0 ; i < approvers.length; i++){
//            System.out.println(approvers[i]);
//        }

//        Log.wtf("ROUNDINFO", String.valueOf(approvers.length));
//        Log.wtf("POS", String.valueOf(position));

        return rowView;
    }
}
