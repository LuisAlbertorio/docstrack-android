package com.pixnabi.luis.docstrack.Utility;

/**
 * Created by Luis on 12/15/2016.
 */

public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
