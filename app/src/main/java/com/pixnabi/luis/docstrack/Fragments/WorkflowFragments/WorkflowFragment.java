package com.pixnabi.luis.docstrack.Fragments.WorkflowFragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.Adapter.WorkflowAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkflowFragment extends Fragment {


    public WorkflowFragment() {
        // Required empty public constructor
    }

    SharedPreferences sharedPreferences;
    public ListView listView;
    WorkflowAdapter adapter;
    TextView emptyWorkflow;

    @Override
    public void onResume() {
        super.onResume();

        MainActivity.titleName.setText("Workflow");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_workflow, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        listView = (ListView)rootView.findViewById(R.id.workflowListView);
        adapter = new WorkflowAdapter(getActivity(), this);
        emptyWorkflow = (TextView)rootView.findViewById(R.id.workflowNoWorkflows);

        MainActivity.titleName.setText("Workflow");

        listView.setAdapter(adapter);

        loadData();

        return rootView;
    }

    public void loadData(){
        MainActivity.progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {

                    Object json = new JSONTokener(result).nextValue();

                    if(json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);
                        MainActivity.progressBar.setVisibility(View.INVISIBLE);
                        emptyWorkflow.setText(jsonObject.getString("error"));


                    }else if(json instanceof JSONArray){
                        JSONArray jsonArray = new JSONArray(result);
                        adapter.setData(jsonArray);
                        listView.setVisibility(View.VISIBLE);
                        MainActivity.progressBar.setVisibility(View.INVISIBLE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("WorkflowShow", sharedPreferences.getString("role", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null));

    }

}
