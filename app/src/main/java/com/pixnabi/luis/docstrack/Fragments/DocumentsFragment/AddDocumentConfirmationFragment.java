package com.pixnabi.luis.docstrack.Fragments.DocumentsFragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddDocumentConfirmationFragment extends Fragment {


    public AddDocumentConfirmationFragment() {
        // Required empty public constructor
    }

    TextView text;
    Button doneButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_add_document_confirmation, container, false);

        text = (TextView)rootView.findViewById(R.id.addDocConfirmationTextView);
        doneButton = (Button)rootView.findViewById(R.id.addDocConfirmationButton);

        return rootView;
    }
}
