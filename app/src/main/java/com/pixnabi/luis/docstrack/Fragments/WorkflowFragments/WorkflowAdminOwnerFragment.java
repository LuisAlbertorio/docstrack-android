package com.pixnabi.luis.docstrack.Fragments.WorkflowFragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.Adapter.WorkflowAdminOwnerApproversAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkflowAdminOwnerFragment extends Fragment {


    public WorkflowAdminOwnerFragment() {
        // Required empty public constructor
    }
    SharedPreferences sharedPreferences;
    Spinner approvers;
    ImageButton approve, reject, stopWorkflow;
    EditText reason;
    TextView docName;
    String docId;
    WorkflowAdminOwnerApproversAdapter adapter;
    JSONArray approversData;

    ProgressDialog progressDialog;
    MainActivity mainActivity;

    String docNum;
    String approverDepartment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_workflow_admin_owner, container, false);
        approvers = (Spinner)rootview.findViewById(R.id.workflowAdminOwnerApproverSpinner);
        approve = (ImageButton)rootview.findViewById(R.id.workflowAdminOwnerApproveButton);
        reject = (ImageButton)rootview.findViewById(R.id.workflowAdminOwnerRejectButton);
        stopWorkflow = (ImageButton)rootview.findViewById(R.id.workflowAdminOwnerStopWorkflowButton);
        reason = (EditText)rootview.findViewById(R.id.workflowAdminOwnerReasonEditText);
        docName = (TextView)rootview.findViewById(R.id.workflowAdminOwnerDocNameTextview);

        MainActivity.titleName.setText("Workflow");
        mainActivity = (MainActivity)getActivity();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Getting Information..");
        progressDialog.show();

        adapter = new WorkflowAdminOwnerApproversAdapter(getActivity());
        approvers.setAdapter(adapter);

        docName.setText(getArguments().getString("docName"));
        docId = getArguments().getString("docId");
        docNum = getArguments().getString("docNum");

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        loadApprovers();

        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovedClicked();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRejectClicked();
            }
        });

        stopWorkflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStopWorkflowClicked();
            }
        });

        return rootview;
    }

    public void loadApprovers(){

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    approversData = jsonArray;
                    adapter.setData(jsonArray);
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("WorkflowApproversSpinner", docId, sharedPreferences.getString("companyId", null));
    }

    public void onApprovedClicked(){
            progressDialog.setMessage("Approving...");
            progressDialog.show();

            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                    try {
                        final JSONObject jsonObject = new JSONObject(result);
                        if(jsonObject.has("project_now_approved")){
                            JSONArray pushId = jsonObject.getJSONArray("project_now_approved");
                            String pushNotiString = "";

                            for(int i=0;i<pushId.length();i++){
                                if(pushNotiString.equals("")){
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = "'" + pushId.getString(i) + "'";
                                    }
                                }else{
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                                    }
                                }
                            }

                            final String finalPushNotiString = pushNotiString;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String pushString = "The document with ID " + docNum + " was approved and closed. Thank you all for your help during this process.";
                                    try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                new OneSignal.PostNotificationResponseHandler() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        Log.i("OneSignal", "postNotification Success: " + response.toString());
                                                    }

                                                    @Override
                                                    public void onFailure(JSONObject response) {
                                                        Log.e("OneSignal", "postNotification Failure: " + response.toString());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            final String projectId = jsonObject.getString("id");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String pushString = "All the documents of the project ID " + projectId + " were approved and closed. Thank you all for your help during this process.";
                                    try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                new OneSignal.PostNotificationResponseHandler() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                    }

                                                    @Override
                                                    public void onFailure(JSONObject response) {
                                                        Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                        if(jsonObject.has("document_now_approved")){
                            JSONArray pushId = jsonObject.getJSONArray("document_now_approved");
                            String pushNotiString = "";

                            for(int i=0;i<pushId.length();i++){
                                if(pushNotiString.equals("")){
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = "'" + pushId.getString(i) + "'";
                                    }
                                }else{
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                                    }
                                }
                            }

                            final String finalPushNotiString = pushNotiString;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String pushString = "The document with ID " + docNum + " was approved and closed . Thank you all for your help during this process.";
                                    try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                new OneSignal.PostNotificationResponseHandler() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                    }

                                                    @Override
                                                    public void onFailure(JSONObject response) {
                                                        Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                        }else

                        if(jsonObject.has("success")){
                            if (!jsonObject.getString("success").equals("null")) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String pushString = null;
                                        try {
                                            pushString = "Document ID " + docNum + " was approved by " + approversData.getJSONObject(approvers.getSelectedItemPosition()).getString("aDepartment") + ". You will receive the document in any moment for your approval.";
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': ['" + jsonObject.getString("success") + "'], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                            new OneSignal.PostNotificationResponseHandler() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                }

                                                @Override
                                                public void onFailure(JSONObject response) {
                                                    Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                }
                                            });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    }
                                }).start();
                            }
                        }

                        progressDialog.dismiss();
                        mainActivity.workflowConfirmationScreen("approve", docName.getText().toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    }
                }).execute("WorkflowSet", sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), docId, "Approve on Behalf", "", approversData.getJSONObject(approvers.getSelectedItemPosition()).getString("aFullName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    public void onRejectClicked(){
        if(!reason.getText().toString().equals("")){
            progressDialog.setMessage("Rejecting...");
            progressDialog.show();
            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if(jsonObject.has("success")){

                            JSONArray pushId = jsonObject.getJSONArray("success");
                            String pushNotiString = "";

                            for(int i=0;i<pushId.length();i++){
                                if(pushNotiString.equals("")){
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = "'" + pushId.getString(i) + "'";
                                    }
                                }else{
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                                    }
                                }
                            }

                            Log.wtf("pushNoti", pushNotiString);

                            final String finalPushNotiString = pushNotiString;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String pushString = null;
                                    try {
                                        pushString = "Document ID " + docNum + " was rejected by " + approversData.getJSONObject(approvers.getSelectedItemPosition()).getString("aDepartment") + ". A new approval process will start shortly.";
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                new OneSignal.PostNotificationResponseHandler() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                    }

                                                    @Override
                                                    public void onFailure(JSONObject response) {
                                                        Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            progressDialog.dismiss();
                            mainActivity.workflowConfirmationScreen("reject", docName.getText().toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    }
                }).execute("WorkflowSet", sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), docId, "Reject on Behalf", reason.getText().toString(), approversData.getJSONObject(approvers.getSelectedItemPosition()).getString("aFullName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(getActivity(), "You must enter a reason", Toast.LENGTH_LONG).show();
        }
    }

    public void onStopWorkflowClicked(){
        if(!reason.getText().toString().equals("")){
            progressDialog.setMessage("Stopping workflow...");
            progressDialog.show();
            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                @Override
                public void processFinish(String result) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if(jsonObject.has("success")){

                            JSONArray pushId = jsonObject.getJSONArray("success");
                            String pushNotiString = "";

                            for(int i=0;i<pushId.length();i++){
                                if(pushNotiString.equals("")){
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = "'" + pushId.getString(i) + "'";
                                    }
                                }else{
                                    if(!pushId.getString(i).equals("null")) {
                                        pushNotiString = pushNotiString + ", '" + pushId.getString(i) + "'";
                                    }
                                }
                            }

                            Log.wtf("pushNoti", pushNotiString);

                            final String finalPushNotiString = pushNotiString;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String pushString = "Document ID " + docNum + " was stopped by the owner. A new approval process will start shortly.";
                                    try {
                                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + pushString + "'}, 'include_player_ids': [" + finalPushNotiString + "], 'small_icon':'docstracklogo', 'large_icon':'docstracklogo'}"),
                                                new OneSignal.PostNotificationResponseHandler() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        Log.i("OneSignalExample", "postNotification Success: " + response.toString());
                                                    }

                                                    @Override
                                                    public void onFailure(JSONObject response) {
                                                        Log.e("OneSignalExample", "postNotification Failure: " + response.toString());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            progressDialog.dismiss();
                            mainActivity.workflowConfirmationScreen("stop", docName.getText().toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).execute("WorkflowSet", sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), docId, "Stop Workflow", reason.getText().toString(), "");
        }else{
            Toast.makeText(getActivity(), "You must enter a reason", Toast.LENGTH_LONG).show();
        }
    }
}
