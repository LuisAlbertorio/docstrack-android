package com.pixnabi.luis.docstrack.SideMenuActivities.SystemCustomList;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.NotificationsAdapter;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class NotificationsActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ListView notiList;
    Button add;
    NotificationsAdapter adapter;
    public static JSONArray data;
    ProgressBar progressBar;
    public static TextView emptyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_notifications);

        notiList = (ListView) findViewById(R.id.notiListView);
        add = (Button) findViewById(R.id.notiAddButton);
        progressBar = (ProgressBar) findViewById(R.id.notiProgressDialog);
        emptyList = (TextView) findViewById(R.id.notiEmptyTextView);

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        adapter = new NotificationsAdapter(this);

        notiList.setAdapter(adapter);

        data = new JSONArray();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddButtonClicked();
            }
        });

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    Object json = new JSONTokener(result).nextValue();
                    if (json instanceof JSONObject) {
                        JSONObject jsonObject = new JSONObject(result);
                        progressBar.setVisibility(View.INVISIBLE);
                        emptyList.setText(jsonObject.getString("empty"));
                        emptyList.setVisibility(View.VISIBLE);
                    } else {
                        JSONArray jsonArray = new JSONArray(result);
                        data = jsonArray;
                        adapter.setData(jsonArray);
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println(result);
            }
        }).execute("notificationsShow", sharedPreferences.getString("role", null), sharedPreferences.getString("companyId", null));
    }

    public void onAddButtonClicked() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_dept_doc_noti);

        TextView title, instructions;
        Button add, cancel;
        final EditText nameDialog;

        title = (TextView) dialog.findViewById(R.id.dialogAddTitleTextView);
        instructions = (TextView) dialog.findViewById(R.id.dialogAddInstruction);
        add = (Button) dialog.findViewById(R.id.dialogAddAddButton);
        cancel = (Button) dialog.findViewById(R.id.dialogAddCancelButton);
        nameDialog = (EditText) dialog.findViewById(R.id.dialogAddEditText);

        dialog.show();

        title.setText("Notification");
        instructions.setText("Enter number of days");
        nameDialog.setInputType(InputType.TYPE_CLASS_NUMBER);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nameDialog.equals("")) {
                    final ProgressDialog progressDialog = new ProgressDialog(NotificationsActivity.this);
                    progressDialog.setMessage("Adding Notification...");
                    progressDialog.show();
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            JSONObject jsonObject = new JSONObject();
                            try {
                                JSONObject resultJson = new JSONObject(result);
                                if (resultJson.has("error")) {
                                    progressDialog.dismiss();
                                    dialog.dismiss();
                                    ErrorDialog.error(NotificationsActivity.this, resultJson.getString("error"));
                                } else {
                                    jsonObject.put("time", nameDialog.getText().toString());
                                    data.put(jsonObject);
                                    adapter.setData(data);
                                    progressDialog.dismiss();
                                    dialog.dismiss();
                                    emptyList.setVisibility(View.INVISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }).execute("notificationsCreate", sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), nameDialog.getText().toString());
                } else {
                    Toast.makeText(NotificationsActivity.this, "Notification cannot be empty", Toast.LENGTH_LONG).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
