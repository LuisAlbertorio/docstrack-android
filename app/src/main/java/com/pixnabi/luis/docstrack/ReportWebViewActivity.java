package com.pixnabi.luis.docstrack;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;


public class ReportWebViewActivity extends AppCompatActivity {

    WebView webView;

    String phpUrl;

    Button downloadButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_web_view);

        webView = (WebView)findViewById(R.id.generateReportsWebView);
        downloadButton = (Button)findViewById(R.id.reportWebViewDownloadButton);

        phpUrl = getIntent().getExtras().getString("url");

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        final String pdf = "http://www.arrozcongandules.com/reportGenerator.php?" + phpUrl;

        String test = "https://docs.google.com/gview?embedded=true&url=" + phpUrl;

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(phpUrl)));
                    }
                }).start();
            }
        });

        webView.loadUrl(test);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
