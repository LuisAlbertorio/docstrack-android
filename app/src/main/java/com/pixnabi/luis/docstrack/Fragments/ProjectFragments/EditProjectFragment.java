package com.pixnabi.luis.docstrack.Fragments.ProjectFragments;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProjectFragment extends Fragment {


    public EditProjectFragment() {
        // Required empty public constructor
    }

    TextView dueDate;
    EditText name, number;
    static TextView newDueDateTextView;
    Button editButton;
    SharedPreferences sharedPreferences;
    String proId;
    MainActivity mainActivity;

    ProgressDialog progressDialog;

    String origName, origNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_project, container, false);

        mainActivity = (MainActivity) getActivity();

        proId = getArguments().getString("id");

        name = (EditText) rootView.findViewById(R.id.editProjectProjectName);
        number = (EditText) rootView.findViewById(R.id.editProjectProjectNumber);
        dueDate = (TextView) rootView.findViewById(R.id.editProjectDueDateTextView);
        editButton = (Button) rootView.findViewById(R.id.editProjectEditButton);
        newDueDateTextView = (TextView) rootView.findViewById(R.id.editProjectNewDueDateTextView);

        MainActivity.titleName.setText("Edit Project");

        progressDialog = new ProgressDialog(getActivity());

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        progressDialog.setMessage("Getting Project Info...");
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                System.out.println(result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    name.setText(jsonObject.getString("name"));
                    number.setText(jsonObject.getString("number"));
                    origName = name.getText().toString();
                    origNumber = number.getText().toString();
                    dueDate.setText(jsonObject.getString("dueDate"));
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).execute("editProjectShowProjectInfo", sharedPreferences.getString("companyId", null), proId);

        newDueDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNewDueDateClicked();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditButtonClicked();
            }
        });

        return rootView;
    }

    public void onNewDueDateClicked() {
        DialogFragment newFragment = new SelectDateFragment();
        newFragment.show(getFragmentManager(), "DatePicker");
    }

    public void onEditButtonClicked() {

        if (name.getText().toString().equals("") || number.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Fields cannot be empty", Toast.LENGTH_LONG).show();
        } else {
            progressDialog.setMessage("Saving Changes...");
            progressDialog.show();

//            if(name.getText().toString().equals(origName) && number.getText().toString().equals(origNumber)){
//                if(!newDueDateTextView.getText().toString().equals("dd/mm/yyyy")){
//                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
//                        @Override
//                        public void processFinish(String result) {
//
//                        }
//                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), newDueDateTextView.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, "null", "null");
//                }
//            }


            if (name.getText().toString().equals(origName) && number.getText().toString().equals(origNumber)) {
                Log.wtf("save", "name, number same");
                if (newDueDateTextView.getText().toString().equals("dd/mm/yyyy")) {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), dueDate.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, "null", "null");
                } else {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), newDueDateTextView.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, "null", "null");
                }

            } else if (number.getText().toString().equals(origNumber)) {
                Log.wtf("save", "number same");
                if (newDueDateTextView.getText().toString().equals("dd/mm/yyyy")) {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), dueDate.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, name.getText().toString(), "null");
                } else {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), newDueDateTextView.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, name.getText().toString(), "null");
                }

            } else if (name.getText().toString().equals(origName)) {
                Log.wtf("numebr changed", "numebr changed");
                if (newDueDateTextView.getText().toString().equals("dd/mm/yyyy")) {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), dueDate.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, "null", number.getText().toString());
                } else {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), newDueDateTextView.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, "null", number.getText().toString());
                }
            } else {
                Log.wtf("both", "both");

                if (newDueDateTextView.getText().toString().equals("dd/mm/yyyy")) {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), dueDate.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, name.getText().toString(), number.getText().toString());
                } else {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            System.out.println(result);
                            try {
                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.has("msg")) {
                                    mainActivity.BackToProjects("normal");
                                    Toast.makeText(getActivity(), "Project updated succesfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } else if (jsonObject.has("error")) {
                                    ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                    progressDialog.dismiss();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("editProjectSet", sharedPreferences.getString("userId", null), newDueDateTextView.getText().toString(), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId", null), proId, name.getText().toString(), number.getText().toString());
                }
            }
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);

        }

        public void populateSetDate(int year, int month, int day) {
//            newDueDate.setText(month + "/" + day + "/" + year);
            newDueDateTextView.setText(month + "/" + day + "/" + year);
        }

    }

}
