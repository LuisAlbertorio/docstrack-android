package com.pixnabi.luis.docstrack.Fragments.DocumentsFragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.pixnabi.luis.docstrack.Adapter.DocumentApproverDropdown;
import com.pixnabi.luis.docstrack.Adapter.DocumentDepartmentDropdown;
import com.pixnabi.luis.docstrack.Adapter.DocumentDocumentTypeDropdown;
import com.pixnabi.luis.docstrack.Adapter.DocumentNotificationDropdown;
import com.pixnabi.luis.docstrack.Adapter.DocumentProjectsDropdown;
import com.pixnabi.luis.docstrack.Adapter.ItemAdapter;
import com.pixnabi.luis.docstrack.Adapter.OwnersAdapter;
import com.pixnabi.luis.docstrack.Adapter.OwnersDocumentAdapter;
import com.pixnabi.luis.docstrack.Adapter.RecyclerAdapter;
import com.pixnabi.luis.docstrack.Adapter.RecyclerCheckAdapter;
import com.pixnabi.luis.docstrack.Adapter.SpinnerAdapter;
import com.pixnabi.luis.docstrack.Adapter.StableArrayAdapter;
import com.pixnabi.luis.docstrack.Fragments.ProjectFragments.AddProjectFragment;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.TestDragDropActivity;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.DynamicListView;
import com.pixnabi.luis.docstrack.Utility.ErrorDialog;
import com.pixnabi.luis.docstrack.Utility.SimpleItemTouchHelperCallback;
import com.woxthebox.draglistview.DragListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.LoggingMXBean;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddDocumentFragment extends Fragment {

    MainActivity mainActivity;

    @Override
    public void onResume() {
        super.onResume();

        count = 0;

        if (resumeowner) {
            resumeowner = false;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                owners = new JSONArray(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            ownersAdapter.setData(owners);
                            progressDialog.dismiss();
                        }
                    }).execute("dropdownOwners", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null));
                }
            }).start();
        }

        if (resumeDept) {
            resumeDept = false;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            documentDepartmentDropdownAdapter = new DocumentDepartmentDropdown(getActivity());
                            departmentSpinner.setAdapter(documentDepartmentDropdownAdapter);
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                dept = jsonArray;
                                documentDepartmentDropdownAdapter.setData(jsonArray, "departments");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            System.out.println("DropdownDepartments: " + result);
                            progressDialog.dismiss();
                        }
                    }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));
                }
            }).start();
        }

        if (resumeNoti) {
            resumeNoti = false;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            documentNotificationDropdown = new DocumentNotificationDropdown(getActivity());
                            notificationSpinner.setAdapter(documentNotificationDropdown);

                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                notifications = jsonArray;
                                documentNotificationDropdown.setData(jsonArray, "notifications");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            System.out.println("DropdownNotifications: " + result);
                            progressDialog.dismiss();
                        }
                    }).execute("DropdownNotifications", sharedPreferences.getString("companyId", null));
                }
            }).start();
        }

        if (resumeDocType) {
            resumeDocType = false;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            documentDocumentTypeDropdown = new DocumentDocumentTypeDropdown(getActivity());
//                            adapter4 = new SpinnerAdapter(getActivity());
                            docTypeSpinner.setAdapter(documentDocumentTypeDropdown);

                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                doctypes = jsonArray;
                                documentDocumentTypeDropdown.setData(jsonArray, "docTypes");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            System.out.println("DropdownDocumentTypes: " + result);
                            progressDialog.dismiss();
                        }
                    }).execute("DropdownDocumentTypes", sharedPreferences.getString("companyId", null));
                }
            }).start();
        }

        if (resumeApprover) {
            resumeApprover = false;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        documentApproverDropdown = new DocumentApproverDropdown(getActivity());
                        approverSpinner.setAdapter(documentApproverDropdown);
                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            approverJSON = jsonArray;
                            documentApproverDropdown.setData(jsonArray, "approvers");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();

                    }
                }).execute("dropdownApproversByDepartment", sharedPreferences.getString("companyId", null), dept.getJSONObject(departmentSpinner.getSelectedItemPosition() - 1).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (resumeProjects) {
            resumeProjects = false;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            documentProjectsDropdown = new DocumentProjectsDropdown(getActivity());
//                        adapter1 = new SpinnerAdapter(getActivity());
                            projectSpinner.setAdapter(documentProjectsDropdown);
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                projects = jsonArray;
                                documentProjectsDropdown.setData(jsonArray, "projects");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            System.out.println("DropdownProjects: " + result);
                            progressDialog.dismiss();
                        }
                    }).execute("DropdownProjects", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null));
                }
            }).start();
        }
    }

    public AddDocumentFragment() {
        // Required empty public constructor
    }

    public static boolean resumeowner = false;
    public static boolean resumeProjects = false;
    public static boolean resumeDept = false;
    public static boolean resumeNoti = false;
    public static boolean resumeDocType = false;
    public static boolean resumeApprover = false;
    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;
    int count = 0;
    Spinner projectSpinner, docTypeSpinner, notificationSpinner, approvalTypeSpinner, departmentSpinner, approverSpinner, ownerSpinner;
    DocumentDepartmentDropdown documentDepartmentDropdownAdapter;
    DocumentNotificationDropdown documentNotificationDropdown;
    DocumentDocumentTypeDropdown documentDocumentTypeDropdown;
    DocumentApproverDropdown documentApproverDropdown;
    DocumentProjectsDropdown documentProjectsDropdown;
    SpinnerAdapter adapter1;
    OwnersDocumentAdapter ownersAdapter;
    String[] approvalTypes = {"Serial Predefined", "Serial Final Approver"};
    String[] approvalTypesDescArray = {"Assigns a linear workflow to be approved by a specified order of users.", "Lets users approve at their discretion and assign a final user to do the final approval."};
    Button addDoc;
    Button approverButton;
    RecyclerView recyclerView;
    RecyclerView recyclerViewFinalApprover;
    RecyclerAdapter adapter;
    ItemTouchHelper.Callback callback;
    EditText docNumberEditText, docNameEditText;
    JSONArray projects, doctypes, notifications, approverJSON, dept, owners;
    ArrayList<String> approver;
    ArrayList<String> departmentNamesForApproversList;
    ArrayList<String> namePlusDepartment;
    ArrayList<Integer> final_approver;
    ArrayList<String> approverUserId;
    TextView approvalOrder, instructions;
    RecyclerCheckAdapter checkAdapter;
    Button addProversShowLayoutButton;
    LinearLayout approverLayout;
    TextView approvalTypesDesc;
    static String dateToCompare;
    static String dateFromProject;
    TextView projectDueDate;

    static TextView dueDateTextView;

    LinearLayout ownerLL;

    String origin;

    ProgressDialog deptprogressDialog;

    LinearLayout dummyFocus;


    boolean firstTimeLoad = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_document, container, false);

        projectSpinner = (Spinner) rootView.findViewById(R.id.addDocumentProjectNameSpinner);
        docTypeSpinner = (Spinner) rootView.findViewById(R.id.addDocumentDocTypeSpinner);
        notificationSpinner = (Spinner) rootView.findViewById(R.id.addDocumentNotificationsSpinner);
        approvalTypeSpinner = (Spinner) rootView.findViewById(R.id.addDocumentApprovalTypeSpinner);
        departmentSpinner = (Spinner) rootView.findViewById(R.id.addDocumentDepartmentSpinner);
        approverSpinner = (Spinner) rootView.findViewById(R.id.addDocumentApproverSpinner);
        addDoc = (Button) rootView.findViewById(R.id.addDocumentAddButton);
        docNumberEditText = (EditText) rootView.findViewById(R.id.addDocumentDocumentNumber);
        docNameEditText = (EditText) rootView.findViewById(R.id.addDocumentDocumentName);
        approverButton = (Button) rootView.findViewById(R.id.addDocumentApproverButton);
        approvalOrder = (TextView) rootView.findViewById(R.id.addDocumentApprovalOrderTextView);
        instructions = (TextView) rootView.findViewById(R.id.addDocumentInstructionTextView);
        dueDateTextView = (TextView) rootView.findViewById(R.id.addDocumentSetDateTextView);
        addProversShowLayoutButton = (Button) rootView.findViewById(R.id.addDocBeforeApprovalLayoutButton);
        approverLayout = (LinearLayout) rootView.findViewById(R.id.addDocApprovalLinearLayout);
        approvalTypesDesc = (TextView) rootView.findViewById(R.id.addDocApprovalTypesDesc);
        projectDueDate = (TextView) rootView.findViewById(R.id.addDocDueDateTextView);
        ownerSpinner = (Spinner) rootView.findViewById(R.id.addDocOwnerSpinner);
        ownerLL = (LinearLayout) rootView.findViewById(R.id.addDocOwnerLinearLayout);
        dummyFocus = (LinearLayout) rootView.findViewById(R.id.dummyFocus);

        mainActivity = (MainActivity) getActivity();

        MainActivity.titleName.setText("Add Document");

        origin = getArguments().getString("origin");

        deptprogressDialog = new ProgressDialog(getActivity());

        projects = new JSONArray();
        doctypes = new JSONArray();
        notifications = new JSONArray();
        dept = new JSONArray();
        approverJSON = new JSONArray();
        owners = new JSONArray();

        approver = new ArrayList<>();
        final_approver = new ArrayList<>();
        departmentNamesForApproversList = new ArrayList<>();
        namePlusDepartment = new ArrayList<>();
        approverUserId = new ArrayList<>();

        checkAdapter = new RecyclerCheckAdapter(getActivity());
        ownersAdapter = new OwnersDocumentAdapter(getActivity());
        ownerSpinner.setAdapter(ownersAdapter);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerApproval);
        recyclerViewFinalApprover = (RecyclerView) rootView.findViewById(R.id.recyclerFinalApprover);
        recyclerViewFinalApprover.setHasFixedSize(true);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        LinearLayoutManager llmfinal = new LinearLayoutManager(getActivity());
        llmfinal.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewFinalApprover.setLayoutManager(llmfinal);
        recyclerViewFinalApprover.setAdapter(checkAdapter);

        ArrayList<String> names = new ArrayList<>();
        adapter = new RecyclerAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        if (sharedPreferences.getString("role", null).equals("Owner")) {
            ownerLL.setVisibility(View.GONE);
        }

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, approvalTypes);
        approvalTypeSpinner.setAdapter(stringArrayAdapter);
        loadDropdowns();

        approvalTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println(position);
                setApproverLayout(position);
                approvalTypesDesc.setText(approvalTypesDescArray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.setMessage("Creating Document..");
                progressDialog.show();
                progressDialog.setCancelable(true);

                if (final_approver.size() > 0) {
                    if (checkAdapter.returnInteger() == 1000 && approvalTypes[approvalTypeSpinner.getSelectedItemPosition()].equals("Serial Final Approver")) {
                        Toast.makeText(getActivity(), "A final approver needs to be selected", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        return;
                    } else if (approvalTypes[approvalTypeSpinner.getSelectedItemPosition()].equals("Serial Final Approver")) {
                        final_approver.set(checkAdapter.returnInteger(), 1);
                    }
                }

                try {
                    Log.wtf("ERROR", projects.getJSONObject(projectSpinner.getSelectedItemPosition()).getString("id") + ", " + docNumberEditText.getText().toString() + ", " + docNameEditText.getText().toString() + ", " + doctypes.getJSONObject(docTypeSpinner.getSelectedItemPosition()).getString("type") + ", " + dueDateTextView.getText().toString() + ", " + notifications.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time") + ", " + dept.getJSONObject(departmentSpinner.getSelectedItemPosition()).getString("name") + ", " + approvalTypes[approvalTypeSpinner.getSelectedItemPosition()] + ", final_approver_string: " + approver.toString() + ", " + final_approver.toString() + ", " + sharedPreferences.getString("userId", null) + ", " + sharedPreferences.getString("companyId", null) + ", " + sharedPreferences.getString("fullName", null) + ", " + owners.getJSONObject(ownerSpinner.getSelectedItemPosition()).getString("oFullName"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {

                    if (sharedPreferences.getString("role", null).equals("Owner")) {

                        if(docTypeSpinner.getSelectedItemPosition() == 0 || notificationSpinner.getSelectedItemPosition() == 0 || departmentSpinner.getSelectedItemPosition() == 0){
                            Toast.makeText(getActivity(), "All Fields must be completed", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }else {

                            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                @Override
                                public void processFinish(String result) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(result);
                                        if (jsonObject.has("error")) {
                                            progressDialog.dismiss();
                                            ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                        } else {
                                            progressDialog.dismiss();

                                            mainActivity.ConfirmProjectCreation(docNameEditText.getText().toString(), origin);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Log.wtf("result", result);
                                }
                            }).execute("DocumentCreate", projects.getJSONObject(projectSpinner.getSelectedItemPosition()).getString("id"), docNumberEditText.getText().toString(), docNameEditText.getText().toString(), doctypes.getJSONObject(docTypeSpinner.getSelectedItemPosition() - 1).getString("type"), dueDateTextView.getText().toString(), notifications.getJSONObject(notificationSpinner.getSelectedItemPosition() - 1).getString("time"), dept.getJSONObject(departmentSpinner.getSelectedItemPosition() - 1).getString("name"), approvalTypes[approvalTypeSpinner.getSelectedItemPosition()], approver.toString(), final_approver.toString(), sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), approverUserId.toString(), sharedPreferences.getString("userId", null));
                        }

                    } else {

                        Log.wtf("POS", String.valueOf(docTypeSpinner.getSelectedItemPosition()));

                        if(docTypeSpinner.getSelectedItemPosition() == 0 || notificationSpinner.getSelectedItemPosition() == 0 || departmentSpinner.getSelectedItemPosition() == 0){
                            Toast.makeText(getActivity(), "All Fields must be completed", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }else{
                            new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                @Override
                                public void processFinish(String result) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(result);
                                        if (jsonObject.has("error")) {
                                            progressDialog.dismiss();
                                            ErrorDialog.error(getActivity(), jsonObject.getString("error"));
                                        } else {
                                            progressDialog.dismiss();

                                            mainActivity.ConfirmProjectCreation(docNameEditText.getText().toString(), origin);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Log.wtf("result", result);
                                }
                            }).execute("DocumentCreate", projects.getJSONObject(projectSpinner.getSelectedItemPosition()).getString("id"), docNumberEditText.getText().toString(), docNameEditText.getText().toString(), doctypes.getJSONObject(docTypeSpinner.getSelectedItemPosition() - 1).getString("type"), dueDateTextView.getText().toString(), notifications.getJSONObject(notificationSpinner.getSelectedItemPosition() - 1).getString("time"), dept.getJSONObject(departmentSpinner.getSelectedItemPosition() - 1).getString("name"), approvalTypes[approvalTypeSpinner.getSelectedItemPosition()], approver.toString(), final_approver.toString(), sharedPreferences.getString("userId", null), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), owners.getJSONObject(ownerSpinner.getSelectedItemPosition()).getString("oFullName"), sharedPreferences.getString("role", null), approverUserId.toString(), owners.getJSONObject(ownerSpinner.getSelectedItemPosition()).getString("id"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        dueDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new AddDocumentFragment.SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        approverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!namePlusDepartment.contains(approverJSON.getJSONObject(approverSpinner.getSelectedItemPosition()).getString("aFullName") + "," + dept.getJSONObject(departmentSpinner.getSelectedItemPosition()-1).getString("name"))) {
                        approver.add(approverJSON.getJSONObject(approverSpinner.getSelectedItemPosition()).getString("aFullName"));
                        departmentNamesForApproversList.add(dept.getJSONObject(departmentSpinner.getSelectedItemPosition()-1).getString("name"));
                        namePlusDepartment.add(approverJSON.getJSONObject(approverSpinner.getSelectedItemPosition()).getString("aFullName") + "," + dept.getJSONObject(departmentSpinner.getSelectedItemPosition()-1).getString("name"));
                        approverUserId.add(approverJSON.getJSONObject(approverSpinner.getSelectedItemPosition()).getString("id"));

                        Log.wtf("AAAAAAH", approver.toString() + ", " + departmentNamesForApproversList.toString() + ", " + approverUserId.toString());
                        adapter.setData(approver, departmentNamesForApproversList, approverUserId, namePlusDepartment);
                        checkAdapter.setData(approver, namePlusDepartment);
                        final_approver.add(0);
                    } else {
                        Toast.makeText(getActivity(), "Approver already added", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        addProversShowLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.wtf("dueDate", dueDateTextView.getText().toString());

                if (dueDateTextView.getText().toString().equals("dd/mm/yyyy")) {
                    Toast.makeText(getActivity(), "You must set a due date for the document", Toast.LENGTH_LONG).show();
                } else {
                    addProversShowLayoutButton.setVisibility(View.GONE);
                    approverLayout.setVisibility(View.VISIBLE);
                }

            }
        });

        docNameEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    docNameEditText.clearFocus();
                    dummyFocus.requestFocus();

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(docNameEditText.getWindowToken(), 0);


//                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

//                    docNameEditText.setCursorVisible(false);

//                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
////                    imm.hideSoftInputFromWindow()
//                    docNameEditText.setFocusable(false);
//                    docNameEditText.setFocusableInTouchMode(true);
                    return true;
                } else {
                    return false;
                }
            }
        });

        projectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    dateFromProject = projects.getJSONObject(position).getString("dueDate");
                    projectDueDate.setText(projects.getJSONObject(position).getString("dueDate"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        departmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!firstTimeLoad) {
                    deptprogressDialog.setMessage("Getting Approvers...");
                    deptprogressDialog.show();
                }
                try {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            if (firstTimeLoad) {
                                Log.wtf("newApproversLayout", result);
                                doneWithDropdowns();
                                firstTimeLoad = false;

                                documentApproverDropdown = new DocumentApproverDropdown(getActivity());
//                                adapter5 = new SpinnerAdapter(getActivity());
                                approverSpinner.setAdapter(documentApproverDropdown);
                                try {
                                    JSONArray jsonArray = new JSONArray(result);
                                    approverJSON = jsonArray;
                                    documentApproverDropdown.setData(jsonArray, "approvers");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                documentApproverDropdown = new DocumentApproverDropdown(getActivity());
//                                adapter5 = new SpinnerAdapter(getActivity());
                                approverSpinner.setAdapter(documentApproverDropdown);
                                try {
                                    JSONArray jsonArray = new JSONArray(result);
                                    approverJSON = jsonArray;
                                    documentApproverDropdown.setData(jsonArray, "approvers");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                deptprogressDialog.dismiss();
                            }
                        }
                    }).execute("dropdownApproversByDepartment", sharedPreferences.getString("companyId", null), dept.getJSONObject(position - 1).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return rootView;
    }

    public void loadDropdowns() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {

                        try {
                            owners = new JSONArray(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        ownersAdapter.setData(owners);
                        doneWithDropdowns();
                    }
                }).execute("dropdownOwners", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null));
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        documentProjectsDropdown = new DocumentProjectsDropdown(getActivity());
                        projectSpinner.setAdapter(documentProjectsDropdown);
                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            projects = jsonArray;
                            documentProjectsDropdown.setData(jsonArray, "projects");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("DropdownProjects: " + result);
                        doneWithDropdowns();
                    }
                }).execute("DropdownProjects", sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null));
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        documentDepartmentDropdownAdapter = new DocumentDepartmentDropdown(getActivity());
                        departmentSpinner.setAdapter(documentDepartmentDropdownAdapter);
                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            dept = jsonArray;
                            documentDepartmentDropdownAdapter.setData(jsonArray, "departments");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("DropdownDepartments: " + result);
                        doneWithDropdowns();
                    }
                }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        documentNotificationDropdown = new DocumentNotificationDropdown(getActivity());
                        notificationSpinner.setAdapter(documentNotificationDropdown);

                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            notifications = jsonArray;
                            documentNotificationDropdown.setData(jsonArray, "notifications");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("DropdownNotifications: " + result);
                        doneWithDropdowns();
                    }
                }).execute("DropdownNotifications", sharedPreferences.getString("companyId", null));
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        documentDocumentTypeDropdown = new DocumentDocumentTypeDropdown(getActivity());
                        docTypeSpinner.setAdapter(documentDocumentTypeDropdown);

                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            doctypes = jsonArray;
                            documentDocumentTypeDropdown.setData(jsonArray, "docTypes");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("DropdownDocumentTypes: " + result);
                        doneWithDropdowns();
                    }
                }).execute("DropdownDocumentTypes", sharedPreferences.getString("companyId", null));
            }
        }).start();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
//                    @Override
//                    public void processFinish(String result) {
//                        adapter5 = new SpinnerAdapter(getActivity());
//                        approverSpinner.setAdapter(adapter5);
//
//                        try {
//                            JSONArray jsonArray = new JSONArray(result);
//                            approverJSON = jsonArray;
//                            adapter5.setData(jsonArray, "approvers");
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        System.out.println("DropdownApprovers: " + result);
//                        doneWithDropdowns();
//                    }
//                }).execute("DropdownApprovers", sharedPreferences.getString("companyId", null));
//            }
//        }).start();
    }

    public void doneWithDropdowns() {
        Log.wtf("count", String.valueOf(count));
        count++;
        if (count == 5) {
            progressDialog.dismiss();
        }
    }

    @SuppressLint("ValidFragment")
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            dateToCompare = month + "/" + day + "/" + year;

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            try {
                Date date1 = sdf.parse(dateToCompare);
                Date date2 = sdf.parse(dateFromProject);

                if (date1.compareTo(date2) > 0) {
                    Toast.makeText(getActivity(), "Document due date cannot be past project date", Toast.LENGTH_LONG).show();
                } else {
                    dueDateTextView.setText(month + "/" + day + "/" + year);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void setApproverLayout(int pos) {
        if (pos == 0) {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerViewFinalApprover.setVisibility(View.GONE);
            approvalOrder.setText("Approval Order: ");
            instructions.setText("Long press user to change order, swipe left or right to remove");
//            recyclerView.setAdapter(adapter);
        } else {
            recyclerViewFinalApprover.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            approvalOrder.setText("Approvers: ");
            instructions.setText("Checkmarked user will be the final approver");
//            recyclerView.setAdapter(checkAdapter);
        }
    }
}
