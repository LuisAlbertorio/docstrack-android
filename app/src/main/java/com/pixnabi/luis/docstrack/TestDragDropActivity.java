package com.pixnabi.luis.docstrack;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.pixnabi.luis.docstrack.Adapter.RecyclerAdapter;
import com.pixnabi.luis.docstrack.Utility.SimpleItemTouchHelperCallback;

import java.util.ArrayList;

public class TestDragDropActivity extends AppCompatActivity {


    RecyclerView recyclerView;

    ArrayList<String> names;
    RecyclerAdapter adapter;
    ItemTouchHelper.Callback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_drag_drop);

        recyclerView = (RecyclerView)findViewById(R.id.testRecycle);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);

        names = new ArrayList<>();

        for(int i=0;i<10;i++){
            names.add("Luis " + i);
        }

        adapter = new RecyclerAdapter(this);
//        adapter.setData(names);

        recyclerView.setAdapter(adapter);

        callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

    }
}
