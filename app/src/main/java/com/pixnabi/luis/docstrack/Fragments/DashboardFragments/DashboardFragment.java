package com.pixnabi.luis.docstrack.Fragments.DashboardFragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.DashboardProjectAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.TestDragDropActivity;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.Text;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    SharedPreferences sharedPreferences;
    MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity)context;
    }

    public DashboardFragment() {
        // Required empty public constructor
    }

    ListView listView;
    TextView statusTextView;
    DashboardProjectAdapter adapter;
    LinearLayout noProjects;
    Button createProject;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        listView = (ListView)rootView.findViewById(R.id.dashboardProjectListView);
        statusTextView = (TextView)rootView.findViewById(R.id.dashboardStatusTextView);
        noProjects = (LinearLayout)rootView.findViewById(R.id.dashboardProjectNoProjectsLinearLayout);
        createProject = (Button)rootView.findViewById(R.id.dashboardProjectCreateButton);
        adapter = new DashboardProjectAdapter(getActivity());

        listView.setAdapter(adapter);

        MainActivity.progressBar.setVisibility(View.VISIBLE);
        MainActivity.titleName.setText("Dashboard");

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        if(sharedPreferences.getString("role", null).equals("Viewer") || sharedPreferences.getString("role", null).equals("Approver")){
            createProject.setVisibility(View.GONE);
        }

        createProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.NewProject("dashboard");
            }
        });

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {

                    Object json = new JSONTokener(result).nextValue();

                    if(json instanceof JSONObject){
                        JSONObject jsonObject = new JSONObject(result);

                        noProjects.setVisibility(View.VISIBLE);


                        MainActivity.progressBar.setVisibility(View.INVISIBLE);


                    }else if(json instanceof JSONArray){
                        JSONArray jsonArray = new JSONArray(result);

                        adapter.setData(jsonArray);

                        MainActivity.progressBar.setVisibility(View.INVISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("dashboardShow", sharedPreferences.getString("role", null), sharedPreferences.getString("userId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("companyId" ,null));

        return rootView;
    }
}
