package com.pixnabi.luis.docstrack.Fragments.DocumentsFragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.SpinnerAdapter;
import com.pixnabi.luis.docstrack.MainActivity;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDocumentFragment extends Fragment {


    public EditDocumentFragment() {
        // Required empty public constructor
    }

    Spinner projectDropdown, documentDropdown, notificationSpinner;
    SpinnerAdapter adapter, adapter2, adapter3;
    SharedPreferences sharedPreferences;
    TextView dueDate, approvalType;
    EditText documentName, documentNumber;
    static TextView newDueDate;
//    static Button setDateButton;
    Button saveButton;
    JSONArray projectsArray, docsArray, notificationsArray;
    ProgressDialog progressDialog;
    String origName, origNumber;
    JSONObject documentObject;
    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_document, container, false);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        projectDropdown = (Spinner)rootView.findViewById(R.id.editDocumentProjectSpinner);
        documentDropdown = (Spinner)rootView.findViewById(R.id.editDocumentDocumentSpinner);
        notificationSpinner = (Spinner)rootView.findViewById(R.id.editDocumentNotificationSpinner);
//        docName = (TextView)rootView.findViewById(R.id.editDocumentDocumentName);
        dueDate = (TextView)rootView.findViewById(R.id.editDocumentDocumentDueDate);
//        setDateButton = (Button)rootView.findViewById(R.id.editDocumentSetDateButton);
        saveButton = (Button)rootView.findViewById(R.id.editDocumentSaveButton);
        newDueDate = (TextView)rootView.findViewById(R.id.editDocumentDocumentNewDueDate);
        approvalType = (TextView)rootView.findViewById(R.id.editDocumentDocumentApprovalType);
        documentName = (EditText) rootView.findViewById(R.id.editDocumentDocumentName);
        documentNumber = (EditText) rootView.findViewById(R.id.editDocumentDocumentNumber);
        mainActivity = (MainActivity)getActivity();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        MainActivity.titleName.setText("Edit Document");

        adapter = new SpinnerAdapter(getActivity());
        adapter2 = new SpinnerAdapter(getActivity());
        adapter3 = new SpinnerAdapter(getActivity());
        projectDropdown.setAdapter(adapter);
        documentDropdown.setAdapter(adapter2);
        notificationSpinner.setAdapter(adapter3);

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    projectsArray = jsonArray;
                    adapter.setData(jsonArray, "editDocumentProjects");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("DocEditProjectShow", sharedPreferences.getString("role", null), sharedPreferences.getString("fullName",null), sharedPreferences.getString("companyId",null));


        projectDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                System.out.println("mooop");

                try {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                docsArray = jsonArray;
                                adapter2.setData(jsonArray, "editDocDocSpinner");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("DocEditDocShow", projectsArray.getJSONObject(position).getString("id"), sharedPreferences.getString("companyId", null));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        documentDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                documentObject = jsonObject;
//                                docName.setText(jsonObject.getString("name"));
                                dueDate.setText(jsonObject.getString("dueDate"));
                                approvalType.setText(jsonObject.getString("approvalType"));
                                documentNumber.setText(jsonObject.getString("num"));
                                origNumber = documentNumber.getText().toString();
                                documentName.setText(jsonObject.getString("name"));
                                origName = documentName.getText().toString();

                                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                                    @Override
                                    public void processFinish(String result) {
                                        try {
                                            JSONArray jsonArray = new JSONArray(result);
                                            notificationsArray = jsonArray;
                                            adapter3.setData(jsonArray, "notifications");

                                            for(int i=0;i<notificationsArray.length();i++){
                                                if(documentObject.getString("notification").equals(notificationsArray.getJSONObject(i).getString("time"))){
                                                    notificationSpinner.setSelection(i);
                                                }
                                            }

                                            progressDialog.dismiss();


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }).execute("DropdownNotifications", sharedPreferences.getString("companyId", null));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("DocumentInfo", docsArray.getJSONObject(position).getString("id"), sharedPreferences.getString("companyId",null));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        setDateButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DialogFragment newFragment = new EditDocumentFragment.SelectDateFragment();
//                newFragment.show(getFragmentManager(), "DatePicker");
//            }
//        });

        newDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new EditDocumentFragment.SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });



        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SaveDocumentChanges();

            }
        });



        return rootView;
    }

    public void SaveDocumentChanges(){
        progressDialog.setMessage("Saving changes...");
        progressDialog.show();

       if(documentName.getText().toString().equals("") || documentNumber.getText().toString().equals("")){
           Toast.makeText(getActivity(), "Fields cannot be empty", Toast.LENGTH_LONG).show();
       }else
       if(documentName.getText().toString().equals(origName) && documentNumber.getText().toString().equals(origNumber)){
           Log.wtf("save", "name, number same");
           if(newDueDate.getText().toString().equals("dd/mm/yyyy")){
               try {
                   new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                       @Override
                       public void processFinish(String result) {
                           progressDialog.dismiss();
                           mainActivity.BackToProjects("normal");
                       }
                   }).execute("EditDocument", documentObject.getString("id"), "null", dueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "null");
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }else{
               try {
                   new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                       @Override
                       public void processFinish(String result) {
                           progressDialog.dismiss();
                           mainActivity.BackToProjects("normal");
                       }
                   }).execute("EditDocument", documentObject.getString("id"), "null", newDueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "null");
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }else if(documentName.getText().toString().equals(origName)){
           Log.wtf("save", "name, date same");
           if(newDueDate.getText().toString().equals("dd/mm/yyyy")) {
               try {
                   new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                       @Override
                       public void processFinish(String result) {
                           progressDialog.dismiss();
                           mainActivity.BackToProjects("normal");
                       }
                   }).execute("EditDocument", documentObject.getString("id"), "null", dueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), documentNumber.getText().toString());
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }else{
               try {
                   new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                       @Override
                       public void processFinish(String result) {
                           progressDialog.dismiss();
                           mainActivity.BackToProjects("normal");
                       }
                   }).execute("EditDocument", documentObject.getString("id"), "null", newDueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), documentNumber.getText().toString());
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }

       }else if(documentNumber.getText().toString().equals(origNumber)){
           Log.wtf("save", "number, date same");
           if(newDueDate.getText().toString().equals("dd/mm/yyyy")) {
               try {
                   new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                       @Override
                       public void processFinish(String result) {
                           progressDialog.dismiss();
                           mainActivity.BackToProjects("normal");
                       }
                   }).execute("EditDocument", documentObject.getString("id"), documentName.getText().toString(), dueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "null");
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }else{
               try {
                   new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                       @Override
                       public void processFinish(String result) {
                           progressDialog.dismiss();
                           mainActivity.BackToProjects("normal");
                       }
                   }).execute("EditDocument", documentObject.getString("id"), documentName.getText().toString(), newDueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), "null");
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }else{
           Log.wtf("save", "all different");
           try {
               new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                   @Override
                   public void processFinish(String result) {
                       progressDialog.dismiss();
                       mainActivity.BackToProjects("normal");
                   }
               }).execute("EditDocument", documentObject.getString("id"), documentName.getText().toString(), newDueDate.getText().toString(), notificationsArray.getJSONObject(notificationSpinner.getSelectedItemPosition()).getString("time").replace(" days", ""), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), documentNumber.getText().toString());
           } catch (JSONException e) {
               e.printStackTrace();
           }
       }
    }

    @SuppressLint("ValidFragment")
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);

        }

        public void populateSetDate(int year, int month, int day) {
//            setDateButton.setText(month + "/" + day + "/" + year);
            newDueDate.setText(month + "/" + day + "/" + year);
        }

    }

}
