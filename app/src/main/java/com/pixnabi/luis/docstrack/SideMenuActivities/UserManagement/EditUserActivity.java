package com.pixnabi.luis.docstrack.SideMenuActivities.UserManagement;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pixnabi.luis.docstrack.Adapter.AddUserRolesAdapter;
import com.pixnabi.luis.docstrack.Adapter.SpinnerAdapter;
import com.pixnabi.luis.docstrack.R;
import com.pixnabi.luis.docstrack.Utility.BackgroundWorker;
import com.pixnabi.luis.docstrack.Utility.ImageToString;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditUserActivity extends AppCompatActivity {

    TextView name, role, email, info;
    EditText phone;
    Spinner roleSpinner, department;
    Button saveUser, refreshPass, deleteUser;
    SharedPreferences sharedPreferences;
    String fname, lname;
    String id;
    CircleImageView profilePicture;
    AddUserRolesAdapter adapter;
    String[] roles = {"", "Owner", "Viewer", "Approver"};
    String[] infoArray = {"This message will display the information, security and accesibility of the role that is selected.", "Owners get full access to the application, except for editing company information and editing or deleting existing users. Dashboard visibility and reporting is limited to Projects and Documents they created.", "Viewers get unique access to all Projects and Documents of the company and are able to generate reports on them, but have no participation in Workflows.", "Approvers are participants of Workflows as assigned by the Owners. Dashboard visibility and reporting is limited to their assigned work."};
    ProgressDialog progressDialog;

    SpinnerAdapter spinnerAdapter;
    JSONArray dataDept;

    String userDept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
        }

        /**
         * Change icons to gray
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setContentView(R.layout.activity_edit_user);

        name = (TextView) findViewById(R.id.editUserName);
        role = (TextView) findViewById(R.id.editUserRole);
        email = (TextView) findViewById(R.id.editUserEmail);
        phone = (EditText) findViewById(R.id.editUserPhone);
        info = (TextView) findViewById(R.id.editUserRoleInfo);
        saveUser = (Button) findViewById(R.id.editUserSaveButton);
        refreshPass = (Button) findViewById(R.id.editUserRefreshPasswordButton);
        roleSpinner = (Spinner) findViewById(R.id.editUserRoleSpinner);
        profilePicture = (CircleImageView) findViewById(R.id.editUserProfilePicture);
        deleteUser = (Button) findViewById(R.id.editUserDeleteButton);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        department = (Spinner) findViewById(R.id.editUserDepartmentSpinner);

        progressDialog = new ProgressDialog(this);

        spinnerAdapter = new SpinnerAdapter(this);
        adapter = new AddUserRolesAdapter(this);
        roleSpinner.setAdapter(adapter);
        department.setAdapter(spinnerAdapter);

        dataDept = new JSONArray();

        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                info.setText(infoArray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String companyId = sharedPreferences.getString("companyId", null);
        String adminRole = sharedPreferences.getString("role", null);
        id = getIntent().getExtras().getString("userId");
        fname = getIntent().getExtras().getString("fname");
        lname = getIntent().getExtras().getString("lname");

        System.out.println("USERID: " + id);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Getting User Info...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    System.out.println(String.valueOf(jsonObject));

                    profilePicture.setImageBitmap(ImageToString.StringToImage(jsonObject.getString("uProfilePicture")));
                    name.setText(jsonObject.getString("fName") + " " + jsonObject.getString("lName"));
                    role.setText(jsonObject.getString("uRole"));
                    email.setText(jsonObject.getString("uEmail"));
                    phone.setText(jsonObject.getString("uPhone"));
                    userDept = jsonObject.getString("uDepartment");

                    new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                        @Override
                        public void processFinish(String result) {
                            try {
                                progressDialog.dismiss();
                                JSONArray jsonArray = new JSONArray(result);
                                dataDept = jsonArray;
                                spinnerAdapter.setData(jsonArray, "addUserDept");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    if (jsonArray.getJSONObject(i).getString("name").equals(userDept)) {
                                        department.setSelection(i);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute("DropdownDepartments", sharedPreferences.getString("companyId", null));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("userEditShow", companyId, adminRole, id);

        saveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickedSaveUser();
            }
        });

        refreshPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefreshPress();
            }
        });

        deleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteClicked();
            }
        });
    }

    public void onClickedSaveUser() {
        String companyId = sharedPreferences.getString("companyId", null);
        String userId = sharedPreferences.getString("userId", null);

        progressDialog.setMessage("Saving Changes...");
        progressDialog.show();

        if (roleSpinner.getSelectedItemPosition() == 0) {
            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            System.out.println(String.valueOf(jsonObject));
                            progressDialog.dismiss();
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("userEditSet", userId, companyId, id, role.getText().toString(), fname, lname, phone.getText().toString(), dataDept.getJSONObject(department.getSelectedItemPosition()).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            System.out.println(String.valueOf(jsonObject));
                            progressDialog.dismiss();
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).execute("userEditSet", userId, companyId, id, roles[roleSpinner.getSelectedItemPosition()], fname, lname, phone.getText().toString(), dataDept.getJSONObject(department.getSelectedItemPosition()).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onRefreshPress() {
        String email = this.email.getText().toString();
        String companyId = sharedPreferences.getString("companyId", null);
        String userId = sharedPreferences.getString("userId", null);

        progressDialog.setMessage("Refreshing user's password...");
        progressDialog.show();

        new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
            @Override
            public void processFinish(String result) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(result);
                    System.out.println(String.valueOf(jsonObject));

                    Toast.makeText(EditUserActivity.this, name.getText().toString() + "'s has been refreshed", Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute("userEditPassRefresh", email, companyId, userId);
    }

    public void onDeleteClicked() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_user_management);
        dialog.show();

        Button cancelButton = (Button) dialog.findViewById(R.id.userManagementDialogCancelButton);
        Button deleteButton = (Button) dialog.findViewById(R.id.userManagementDialogDeleteButton);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.setMessage("Deleting User...");
                progressDialog.show();

                new BackgroundWorker(new BackgroundWorker.AsyncResponse() {
                    @Override
                    public void processFinish(String result) {
                        System.out.println(result);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                        progressDialog.dismiss();
                        finish();

                    }
                }).execute("userDelete", email.getText().toString(), sharedPreferences.getString("companyId", null), sharedPreferences.getString("fullName", null), sharedPreferences.getString("role", null));
            }
        });
    }
}
